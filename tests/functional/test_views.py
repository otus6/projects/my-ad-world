

class TestViews:
    """Class for functional testing of application views.
    """
    def test_home(self, test_client):
        """Tests for response results of home view

        Args:
            test_client (FlaskClient): Generator with test client.
        """
        get_response = test_client.get("/")
        post_response = test_client.post("/")
        assert get_response.status_code == 200
        assert post_response.status_code == 405


    def test_query(self, test_client):
        """Tests for response results of query view

        Args:
            test_client (FlaskClient): Generator with test client.
        """
        get_response = test_client.get("/query")
        post_response = test_client.post("/query")
        assert get_response.status_code == 200
        assert post_response.status_code == 200


# def test_home(test_client):
#     response = test_client.get("/")
#     assert response.status_code == 200


# def test_query(test_client):
#     response = test_client.get("/query")
#     assert response.status_code == 200
