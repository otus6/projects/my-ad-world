# pylint: disable=import-error, no-member
from app import create_app
from app.logger import get_app_mode
from app.models.base import db
from app.models.choice import Choice
from faker import Faker
import os
from pytest import fixture


# app = create_app("testing")
# get_app_mode("testing")
os.environ["FLASK_ENV"] = "testing"
app = create_app()

@fixture()
def test_client():
    """Create a test client using the Flask application configured for testing

    Yields:
        (FlaskClient): Tests generator.
    """
    with app.test_client() as testing_client:
        app.testing = True
        yield testing_client # this is where the testing happens!


@fixture()
def prepare_db():
    """Create fake data for testing database.

    Yields:
        _type_: _description_
    """
    with app.app_context():
        db.create_all()
        db.session.query(Choice).delete()
        db.session.commit()

    fake = Faker()
    for _ in range(3):
        name = fake.name()
        choice = Choice(name=name)
        with app.app_context():
            db.session.add(choice)
            db.session.commit()

    for _ in range(3):
        name = fake.name()
        text = fake.text()
        choice = Choice(name=name, extra=text)
        with app.app_context():
            db.session.add(choice)
            db.session.commit()

    with app.app_context():
        yield  # this is where the testing happens!
