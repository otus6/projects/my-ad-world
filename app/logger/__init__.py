from loguru import logger
import sys


config_logger = {
    "handlers": [
        {"sink": sys.stderr},
    ]
}
logger.configure(**config_logger)   # type: ignore
