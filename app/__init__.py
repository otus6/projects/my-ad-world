# pylint: disable=no-member, import-outside-toplevel
from app.models import User
from flask import Flask
from flask_bootstrap import Bootstrap5
from flask_login import LoginManager
from os import environ


def create_app() -> Flask:
    """It creates pre-configured Flask application instance.

    Returns:
        Flask: Pre-configured instance of Flask application.
    """
    from app.config import DevelopmentConfig, ProductionConfig, TestingConfig, Config

    app = Flask(__name__)
    bootstrap = Bootstrap5(app)             # pylint: disable=unused-variable
    config: Config = ProductionConfig()
    app_mode = environ.get("FLASK_ENV")
    if app_mode == "development":
        config = DevelopmentConfig()
    elif app_mode == "testing":
        config = TestingConfig()
    app.config.from_object(config)
    app.jinja_env.add_extension("jinja2.ext.loopcontrols")

    from app.models.base import db, migrate

    db.init_app(app)
    migrate.init_app(app, db)

    login_manager = LoginManager()
    login_manager.init_app(app)
    login_manager.login_view = "/login"

    @login_manager.user_loader
    def load_user(user_id):
        return db.session.query(User).get(user_id)

    from app.views import upload, home, login, logout, signup, advert

    app.register_blueprint(upload)
    app.register_blueprint(home)
    app.register_blueprint(login)
    app.register_blueprint(logout)
    app.register_blueprint(signup)
    app.register_blueprint(advert)

    return app
