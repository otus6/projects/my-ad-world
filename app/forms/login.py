from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField
from wtforms.validators import DataRequired, Email



class LoginForm(FlaskForm):
    email = StringField("Email", validators=[DataRequired(), Email()],
        render_kw={
            "class": "form-control ",
            "placeholder": "Email or username",
            "type": "email",
            "autofocus": True,
            "id": "email"
            }
            )
    password = PasswordField("Password", validators=[DataRequired()],
        render_kw={
            "class": "form-control mt-1",
            "placeholder": "Password",
            "type": "password",
            "id": "password"
            }
    )
    remember = BooleanField("Remember me",
        render_kw={
            "type": "checkbox"
        }
    )
    submit = SubmitField("Sign in",
        render_kw={
            "class": "btn btn-lg btn-primary",
            "Type": "submit"
            }
    )
