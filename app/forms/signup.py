# pylint: disable=wrong-import-position, import-error
from pathlib import Path
import sys
projectdir = Path(__file__).parents[2]
sys.path.append(str(projectdir))
from app.models import User

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired, Email, Regexp, EqualTo, ValidationError, Length, Optional


class SignupForm(FlaskForm):
    firstname = StringField("Firstname",
        validators=[
            Length(
                min=3,
                max=25,
                message="Firstname length must be between %(min)d and %(max)dcharacters"
                ),
            Optional()
            ],
        render_kw={
            "class": "form-control",
            "placeholder": "Enter firstname (optional)",
            "type": "text",
            "autofocus": True
            }
        )
    surename = StringField("Surename",
        validators=[
            Length(
                min=3,
                max=25,
                message="Surename length must be between %(min)d and %(max)dcharacters"
                ),
            Optional()
            ],
        render_kw={
            "class": "form-control mt-1",
            "placeholder": "Enter surename (optional)",
            "type": "text"
            }
        )
    email = StringField("Email",
        validators=[
            DataRequired(),
            Email(),
            Length(
                min=6,
                max=35,
                message="Email length must be between %(min)d and %(max)dcharacters"
                )
            ],
            render_kw={
                "class": "form-control mt-1",
                "placeholder": "Enter email",
                "type": "email",
                "id": "email"
                }
            )
    password = PasswordField("Password",
        # validators=[DataRequired(), Regexp(regexp)],
        validators=[
            DataRequired(),
            EqualTo("confirm", message="Passwords must match"),
            Length(
                min=8,
                max=35,
                message="Password length must be between %(min)d and %(max)dcharacters")
            ],
            render_kw={
                "class": "form-control mt-1",
                "placeholder": "Enter password",
                "type": "password"
                }
            )
    confirm = PasswordField("Repeat Password",
            render_kw={
                "class": "form-control mt-1",
                "placeholder": "Confirm password",
                "type": "password"
                }
    )
    # confirm = PasswordField("Confirm password",
    #     validators=[DataRequired(), Regexp(regexp), EqualTo("password_reg")],
    #         render_kw={
    #             "class": "form-control",
    #             "placeholder": "Enter password",
    #             "type": "password"
    #             }
    #         )
    submit = SubmitField("Sign Up",
        render_kw={
            "class": "btn btn-lg btn-primary",
            "Type": "submit"
            }
        )

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user:
            raise ValidationError("User with this email is already registered. Please use another one.")
        else:
            pass
