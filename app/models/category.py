from .base import BaseModel
from sqlalchemy import Integer, String, Column, ForeignKey
from sqlalchemy.orm import relationship


class Category(BaseModel):
    __tablename__ = "category"
    id = Column(Integer, primary_key=True)
    name = Column(String(50), nullable=True)
    subcategory = relationship("Subcategory", backref="parent")

    def __repr__(self) -> str:
        return f"<Category {self.name} >"


class Subcategory(BaseModel):
    __tablename__ = "subcategory"
    id = Column(Integer, primary_key=True)
    name = Column(String(50), nullable=True)
    category_id = Column(Integer, ForeignKey("category.id"))
    advert = relationship("Advert", backref="advert_subcategory")

    def __repr__(self) -> str:
        return f"<Subcategory {self.name} >"
