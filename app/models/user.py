from .base import BaseModel
from datetime import datetime
from flask_login import UserMixin
from sqlalchemy import Integer, String, Column, DateTime
from sqlalchemy.orm import relationship
from werkzeug.security import generate_password_hash, check_password_hash


class User(BaseModel, UserMixin):
    __tablename__ = "user"       
    id = Column(Integer, primary_key=True)
    firstname = Column(String(50), nullable=True)
    surename = Column(String(50), nullable=True)
    password = Column(String(120), nullable=False)
    email = Column(String(120), nullable=False)
    birth = Column(DateTime, nullable=True)
    phone = Column(String(50), nullable=True)
    icon = Column(String(150), default="roger-berry-avatar-placeholder.png")
    city = Column(String(50), nullable=True)
    postcode = Column(Integer, nullable=True)
    address = Column(String(50), nullable=True)
    registered = Column(DateTime, default=datetime.utcnow())
    advert = relationship("Advert", backref="advert_owner")


    def set_password(self, password) -> str:
        self.password = generate_password_hash(password)

    def check_password(self, password) -> bool:
        return check_password_hash(self.password, password)

    def __repr__(self) -> str:
        return f'<User {self.firstname} {self.email}>'
