from .base import BaseModel
from datetime import datetime
from sqlalchemy import Integer, String, Column, ForeignKey, Numeric, DateTime


class Advert(BaseModel):
    __tablename__ = "advert"
    id = Column(Integer, primary_key=True)
    name = Column(String(50), nullable=True)
    subcategory_id = Column(Integer, ForeignKey("subcategory.id"))
    user_id = Column(Integer, ForeignKey("user.id"))
    description = Column(String(2000), nullable=False)
    price = Column(Numeric, nullable=True)
    photos = Column(String(2000))
    location = Column(String(50), nullable=True)
    zip = Column(Integer, nullable=True)
    address = Column(String(50), nullable=True)
    published = Column(DateTime, default=datetime.utcnow())


    def __repr__(self):
        return f"<Advert {self.name} >"
