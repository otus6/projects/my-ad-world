# pylint: disable=too-few-public-methods
from .base import Config


class ProductionConfig(Config):
    """Example of configuration that can be suitable to production.

    Args:
        Config (_type_): Parent class that has base configuration for that class.
    """
    DEBUG = False
