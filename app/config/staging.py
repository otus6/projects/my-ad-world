# pylint: disable=too-few-public-methods
from .base import Config


class StagingConfig(Config):
    """Example of configuration that can be suitable to staging.

    Args:
        Config (_type_): Parent class that has base configuration for that class.
    """
    DEBUG = True
