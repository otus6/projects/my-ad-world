from os import environ
try:
    from .local_settings import DATABASE_URL, SECRET_KEY, basedir
except ModuleNotFoundError:
    DATABASE_URL = environ.get("DATABASE_URL_CI")
    SECRET_KEY = environ.get("SECRET_KEY_CI")
    basedir = None


class Config():
    """Base application config class which will be inheritanced by other config classes."""
    CSRF_ENABLED = True
    DEBUG = False
    DEVELOPMENT = False
    SECRET_KEY = SECRET_KEY
    SQLALCHEMY_DATABASE_URI = DATABASE_URL
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ENGINE_OPTIONS = {"pool_pre_ping": True}
    SQLALCHEMY_POOL_SIZE = 10
    SQLALCHEMY_MAX_OVERFLOW = 20
    SQLALCHEMY_POOL_RECYCLE = 1800
    TESTING = False

    UPLOAD_FOLDER = f"{basedir}/../static/uploads"
    ALLOWED_EXTENSIONS  = ["png", "jpg", "jpeg", "gif"]
    # MAX_IMAGE_FILESIZE = 0.5 * 1024 * 1024
    # SQLALCHEMY_BINDS = {"world": MYSQL_URL}

    RECAPTCHA_USE_SSL = False
    RECAPTCHA_PUBLIC_KEY = 'public'
    RECAPTCHA_PRIVATE_KEY = 'private'
    RECAPTCHA_OPTIONS = {'theme': 'white'}

    def change_db_uri(self, old_uri: str, app_mode: str) -> str:
        """Change SQLALCHEMY_DATABASE_URI to suitable current application mode and DBMS

        Args:
            old_uri (str): DATABASE_URL from local_settings.py
            app_mode (str): Current type of application mode

        Raises:
            ValueError: When database name doesn't have '-' character.
            ValueError: When database engine (DBMS) is not SQLITE, POSTRESQL and MYSQL.

        Returns:
            str: Changed SQLALCHEMY_DATABASE_URI
        """
        old_db_name = old_uri.split("/")[-1]
        base_uri = old_uri[:-len(old_db_name)]
        if old_uri.lower().startswith("sqlite"):
            prefix_idx = old_db_name.rindex(".")
            basename_db = old_db_name[:prefix_idx]
            new_db_name = basename_db + "-" + app_mode + ".db"
        elif old_uri.lower().startswith("postgresql") or old_uri.lower().startswith("mysql"):
            try:
                prefix_idx = old_db_name.rindex("-")
                basename_db = old_db_name[:prefix_idx]
            except ValueError:
                basename_db = old_db_name
            new_db_name = basename_db + "-" + app_mode
        else:
            engine = old_uri.split(":")[0]
            raise ValueError(f"Database engine '{engine}' in unknown")
        new_uri = base_uri + new_db_name
        return new_uri
