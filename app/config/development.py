# pylint: disable=too-few-public-methods
from .base import Config
from os import environ


class DevelopmentConfig(Config):
    """Configuration that is suitable to development.

    Args:
        Config (_type_): Parent class that has base configuration for that class.
    """
    DEVELOPMENT = True
    DEBUG = True
    SQLALCHEMY_DATABASE_URI: str = Config().change_db_uri(
        Config.SQLALCHEMY_DATABASE_URI,
        environ.get("FLASK_ENV")
        )
