# pylint: disable=wrong-import-position, import-error
from pathlib import Path
import sys
projectdir = Path(__file__).parents[2]
sys.path.append(str(projectdir))
from app.forms import LoginForm
from app.models import User

from flask import abort, Blueprint, flash, redirect, render_template, url_for
from flask_login import login_user, current_user
from jinja2 import TemplateNotFound


login = Blueprint("login", __name__, template_folder="templates", static_folder="static")


@login.route("/login", methods=["GET", "POST"])
def show():
    if current_user.is_authenticated:
        flash("You are already logged in")
        return redirect(url_for("home.show"))

    form = LoginForm()
    try:
        return render_template("login.html", form=form)
    except TemplateNotFound:
        abort(404)


@login.route("/process-login", methods=["POST"])
def process():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and user.check_password(form.password.data):
            login_user(user)
            flash("You are successfully authorized")
            return redirect(url_for("home.show"))

        flash("Wrong email or password")
        return redirect(url_for("login.show"))
