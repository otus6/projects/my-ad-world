# pylint: disable=wrong-import-position, import-error
from pathlib import Path
import sys
projectdir = Path(__file__).parents[2]
sys.path.append(str(projectdir))
from app.forms import SignupForm
from app.logger import logger
from app.models import User
from app.models.base import db

from flask import abort, Blueprint, current_app, flash, redirect, render_template, url_for
from flask_login import current_user
from jinja2 import TemplateNotFound
    

signup = Blueprint("signup", __name__, template_folder="templates", static_folder="static")


@signup.route("/signup")
def show():
    if current_user.is_authenticated:
        flash("You are already logged in")
        return redirect(url_for("home.show"))

    title = "Sign Up"
    form = SignupForm()
    try:
        return render_template(
            "signup.html",
            title=title,
            form=form,
            )
    except TemplateNotFound:
        abort(404)



@signup.route("/process-signup", methods=["POST"])
def process():
    form = SignupForm()
    if form.validate_on_submit():
        new_user = User(
            email=form.email.data,
            firstname=form.firstname.data,
            surename=form.surename.data
            )
        new_user.set_password(form.password.data)
        with current_app.app_context():
            db.session.add(new_user)            # pylint: disable=no-member
            db.session.commit()                 # pylint: disable=no-member

        flash("You are successfully signed up")
        return redirect(url_for("login.show"))

    # try:
    #     error = form.errors["email"][0]
    # except:
    #     try:
    #         error = form.errors["firstname"][0]
    #     except:
    #         password = form.password.data
    #         password_confirm = form.confirm.data
    #         if not password == password_confirm:
    #             flash("Пароли не совпадают. Повторите ввод")
    #             return redirect(url_for("registration"))
    #         error = "Пароль должен содержать хотя бы одну заглавную букву, хотя бы одну цифру и быть не менее 8 символов"

    # logger.info(error)
    # flash(error)
    return redirect(url_for("signup.show"))
