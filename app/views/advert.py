# pylint: disable=wrong-import-position, import-error
from pathlib import Path
import sys
projectdir = Path(__file__).parents[2]
sys.path.append(str(projectdir))
from app.models import Subcategory, Advert

from flask import abort, Blueprint, current_app, flash, redirect, render_template, request, url_for, send_from_directory
from flask_login import login_required, current_user
from jinja2 import TemplateNotFound


advert = Blueprint("advert", __name__, template_folder="templates", static_folder="static")

@advert.route('/pets/<int:ad_id>')
@login_required
def show(ad_id):
    content = Advert.query.get_or_404(ad_id)
    try:
        return render_template('advert.html', advert=content)
    except TemplateNotFound:
        abort(404)
