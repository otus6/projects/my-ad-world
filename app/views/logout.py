from flask import Blueprint, flash, redirect, url_for
from flask_login import logout_user


logout = Blueprint("logout", __name__, template_folder="templates", static_folder="static")


@logout.route("/logout")
def show():
    logout_user()
    flash("You are successfully logged out")
    return redirect(url_for("login.show"))
