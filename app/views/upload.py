# pylint: disable=inconsistent-return-statements
from flask import abort, Blueprint, current_app, flash, redirect, render_template, request, url_for, send_from_directory
from jinja2 import TemplateNotFound
from os.path import join
from werkzeug.utils import secure_filename


upload = Blueprint("upload", __name__, template_folder="templates", static_folder="static")


def allowed_file(filename):
    return "." in filename and \
           filename.rsplit(".", 1)[1].lower() in current_app.config["ALLOWED_EXTENSIONS"]

@upload.route('/uploads/<name>')
def download_file(name):
    return send_from_directory(current_app.config["UPLOAD_FOLDER"], name)


@upload.route("/upload", methods=["GET", "POST"])
def upload_file():
    if request.method == "POST":
        # check if the post request has the file part
        if "file" not in request.files:
            flash("No file part")
            return redirect(request.url)
        file = request.files["file"]
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == "":
            flash("No selected file")
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            # Здесь надо класть filename в базу
            file.save(join(current_app.config["UPLOAD_FOLDER"], filename))
            return redirect(url_for("download_file", name=filename))
    try:
        return render_template("upload.html")
    except TemplateNotFound:
        abort(404)
