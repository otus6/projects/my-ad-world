# pylint: disable=wrong-import-position, import-error
from pathlib import Path
import sys
projectdir = Path(__file__).parents[2]
sys.path.append(str(projectdir))
from app.models import Advert

from flask import abort, Blueprint, current_app, flash, redirect, render_template, request, url_for, send_from_directory
from flask_login import login_required, current_user
from jinja2 import TemplateNotFound


home = Blueprint("home", __name__, template_folder="templates", static_folder="static")


@home.route("/")
@login_required
def root():
    return redirect(url_for("home.show"))

@home.route("/home", methods=["GET", "POST"])
@login_required
def show():
    ads = Advert.query.order_by(Advert.published.desc()).all()
    try:
        return render_template("home.html", name=current_user.firstname, email=current_user.email, ads=ads[:3])
    except TemplateNotFound:
        abort(404)
