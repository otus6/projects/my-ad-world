# pylint: disable=import-error
from app import create_app
from app.logger import get_app_mode


app = create_app("testing")
get_app_mode()


class Testviews:
    
    def setup(self):
        app.testing = True
        self.client = app.test_client()


    def test_home(self):
        response = self.client.get("/")
        assert response.status_code == 200


    def test_query(self):
        response = self.client.get("/query")
        assert response.status_code == 200


    def teardown(self):
        pass