# pylint: disable=no-member, wrong-import-position, duplicate-code
import sys
sys.path.append("/home/geezy/Projects/Flask/my-ad-world")
from app import create_app
from app.logger.base import get_app_mode
from app.models.base import db
import argparse


def init_parser() -> argparse.Namespace:
    """It initializes argument parser for module.

    Returns:
        Namespace: Passed arguments to the module.
    """
    parser = argparse.ArgumentParser(
        description="This module creates databases for this flask application."
    )
    parser.add_argument(
        "-d",
        "--dev",
        help=(
            "It enables development mode of application. "
            "WARNING! By default (without this key) it works in production mode."
            ),
        action="store_true",
    )
    parser.add_argument(
        "-t",
        "--testing",
        help=(
            "It enables testing mode of application. "
            "WARNING! By default (without this key) it works in production mode."
            ),
        action="store_true",
    )
    args = parser.parse_args()

    return args


def main() -> None:
    """Main function, that runs create_db.py module.
    """
    args = init_parser()
    app_mode = "production"
    if args.dev:
        app_mode = "development"
    elif args.testing:
        app_mode = "testing"
    get_app_mode(app_mode)
    app = create_app(app_mode)


    # one = Choice(name=f"choice 1 for {app_mode}", extra=f"additional info about 1 for {app_mode}")
    # two = Choice(name=f"choice 2 for {app_mode}", extra=f"additional info about 2 for {app_mode}")
    # three = Choice(name=f"choice 3 for {app_mode}", extra=f"additional info about 3 for {app_mode}")

    with app.app_context():
        db.create_all()

    db_name = "app"
    if app_mode != "production":
        db_name += f"-{app_mode}"
    print(f"[OK] Database '{db_name}.db' has successfully created or initialized")

    # with app.app_context():
    #     for item in (one,two,three):
    #         db.session.add(item)
    #     db.session.commit()
    # print("[OK] Raws were written to database successfully")


if __name__ == '__main__':
    main()
