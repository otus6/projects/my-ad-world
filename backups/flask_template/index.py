@app.route("/", methods=["GET", "POST"])
def index():
    form = ChoiceForm()
    if form.validate_on_submit():
        return f"<h1>{form.opts.data}</h1>"

    return render_template("index.html", form=form)
