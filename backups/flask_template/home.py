# pylint: disable=inconsistent-return-statements
from app.logger import logger, catch_exception
from flask import Blueprint, render_template, abort
from jinja2 import TemplateNotFound


home = Blueprint("home", __name__, template_folder="templates", static_folder="static")


@home.route("/")
@home.route("/home")
@home.route("/index")
@catch_exception(logger)
def show():
    """It returns rendered html.

    Returns:
        str: HTML webpage.
    """
    context = {
        "name": None,
        "text": None,
        "extra": None
    }
    try:
        return render_template("index.html", **context)
    except TemplateNotFound:
        abort(404)
