from .local_settings import DATABASE_URL, SECRET_KEY


class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = SECRET_KEY
    SQLALCHEMY_DATABASE_URI = DATABASE_URL
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    ### Below is postgres and mysql stuff from example
    # LOGFILE = "lp-lms.log"
    # SQLALCHEMY_BINDS = {"world": MYSQL_URL}
    # IMAGE_UPLOADS = "/home/geezy/Projects/lms-on-dev/webapp/static/img/uploads"
    # ALLOWED_IMAGE_EXTENSIONS = ["PNG", "JPG", "JPEG", "GIF"]
    # MAX_IMAGE_FILESIZE = 0.5 * 1024 * 1024


class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
    LOG_LEVEL = 'DEBUG'
    LOG_BACKTRACE = True


class TestingConfig(Config):
    TESTING = True
