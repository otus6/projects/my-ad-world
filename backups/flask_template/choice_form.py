from app.models.choice import choice_query
from flask_wtf import FlaskForm
from wtforms_sqlalchemy.fields import QuerySelectField


class ChoiceForm(FlaskForm):
    """Form for query view.

    Args:
        FlaskForm (_type_): Flask form base class.
    """
    opts = QuerySelectField(
        query_factory = choice_query,
        allow_blank = True,
        get_label = "name",
        blank_text = "Choose one of the items",
    )
