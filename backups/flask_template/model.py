from flask_sqlalchemy import SQLAlchemy


db = SQLAlchemy()


class Choice(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    extra = db.Column(db.String(50))

    def __repr__(self):
        return f'[Choice {self.name}]'

def choice_query():
    return Choice.query
