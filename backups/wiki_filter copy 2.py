# pylint: disable=wrong-import-position, import-error
from base import get_headers, get_proxy, make_request
from bs4 import BeautifulSoup
from json import dumps
import re


def get_html(URL):
    response = make_request(
        URL,
        headers=get_headers(),
        proxies=get_proxy()
        )
    return response.content


def get_soup(URL):
    html = get_html(URL)
    soup = BeautifulSoup(html, "html.parser")
    return soup


def get_not_breeds(URL):
    soup = get_soup(URL)
    try:
        uls = soup.find("span", {"id": "Siehe_auch"}).find_all_next("ul")
    except AttributeError:
        uls = soup.find("span", {"id": "Weblinks"}).find_all_next("ul")
    not_breeds = [li.text.strip() for ul in uls for li in ul if li.text.strip()]
    return not_breeds


def get_breed_from_table(tables):
    for table in tables:
        # WA for cats wasted table (wasted table in the end)
        if table["class"] == ['wikitable', 'zebra']:
            continue
        for tr_tag in table.find_all("tr"):
            breed = tr_tag.find("td")
            if breed:
                breed = re.sub(r"\[\d\]", "", breed.text.strip())
                return breed


def get_breed_from_ul(uls):
    for ul_tag in uls:
        for li_tag in ul_tag:
            if li_tag.text.strip():
                try:
                    li_tag["class"]
                except KeyError:
                    a_tag = li_tag.find("a")
                    try:
                        breed = a_tag.text
                    except AttributeError:
                        continue
                    if "http://" in a_tag["href"]:
                        break
                    breed = re.sub(r"\[\d+\]|\[FAO\s\d+\]", "", breed.strip())
                    if breed:
                        return breed
                else:
                    # WA for cows (wasted ul in the beginning)
                    regex = re.compile(r"toc\w+\-\d+")
                    if all(regex.match(item) for item in li_tag["class"]):
                        continue
# fake.cats_breed()
# fake.cats_desc()
# fake.cats_photo()

def get_breeds(url):
    soup = get_soup(url)
    uls = soup.find("div", {"class": "mw-parser-output"}).find_all("ul")
    tables = soup.find_all("table", {"class": "wikitable"})

    unfiltered = []
    if tables:
        for table in tables:
            # WA for cats wasted table (wasted table in the end)
            if table["class"] == ['wikitable', 'zebra']:
                continue
            for tr_tag in table.find_all("tr"):
                breed = tr_tag.find("td")
                if breed:
                    breed = re.sub(r"\[\d\]", "", breed.text.strip())
                    unfiltered.append(breed)
    else:
        for ul_tag in uls:
            for li_tag in ul_tag:
                if li_tag.text.strip():
                    try:
                        li_tag["class"]
                    except KeyError:
                        a_tag = li_tag.find("a")
                        try:
                            breed = a_tag.text
                        except AttributeError:
                            continue
                        if "http://" in a_tag["href"]:
                            break
                        breed = re.sub(r"\[\d+\]|\[FAO\s\d+\]", "", breed.strip())
                        if breed:
                            unfiltered.append(breed)
                    else:
                        # WA for cows (wasted ul in the beginning)
                        regex = re.compile(r"toc\w+\-\d+")
                        if all(regex.match(item) for item in li_tag["class"]):
                            continue

    not_breeds = get_not_breeds(url)
    breeds = list((filter(lambda breed: breed not in not_breeds, unfiltered)))
    unique_breeds = list({breed:breed for breed in breeds}.values())
    print(unique_breeds)
    print("+++++++++++++++++++++++++++")
    return unique_breeds


def get_animal(a_tag):
    name = a_tag.text.split()[-1].replace("rassen","")
    url = f"https://de.wikipedia.org{a_tag['href']}"
    if name == "Liste seltener Hunderassen":
        return {}
    animal =  {
        "name": name,
        "breeds": get_breeds(url),
        }
    return animal


def main():
    soup = get_soup("https://de.wikipedia.org/wiki/Liste_von_Haustierrassen")
    tr_list = soup.find("table", {"class": "wikitable"}).find("tbody").find_all("tr")
    animals = [
        get_animal(a) for tr in tr_list for a in tr.find_all("a") if a.text.startswith("Liste")
        ]
    # print(dumps(animals, indent=4, ensure_ascii=False))

if __name__ == '__main__':
    main()
    # get_filter()
