# pylint: disable=wrong-import-position, import-error, invalid-name
from base import get_headers, get_proxy, make_request, logger
from bs4 import BeautifulSoup
from pathlib import Path
import re
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from seleniumwire.webdriver import Firefox
import sys
from urllib.parse import unquote
from time import sleep
import polling2


def get_html(url):
    response = make_request(
        url,
        headers=get_headers(),
        proxies=get_proxy()
        )
    return response.content


def get_soup(url):
    html = get_html(url)
    soup = BeautifulSoup(html, "html.parser")
    return soup


def match_url(url):
    regex = re.compile(r"https://de.wikipedia.org/wiki/(\w+|(\w+-\w+)+)$")
    match = re.match(regex, url)
    if match:
        return True


def page_exist(url):
    if match_url(url):
        response = make_request(
            url,
            headers=get_headers(),
            proxies=get_proxy()
            )
        if response.status_code == 200:
            return True
    return False


def scroll_shim(passed_in_driver, tag_object):
    x = tag_object.location["x"]
    y = tag_object.location["y"]
    scroll_by_coord = f"window.scrollTo({x},{y});"
    scroll_nav_out_of_way = "window.scrollBy(0, -120);"
    passed_in_driver.execute_script(scroll_by_coord)
    passed_in_driver.execute_script(scroll_nav_out_of_way)


def get_driver(url):
    proxy = get_proxy(selenium=True)

    options = Options()
    options.headless = True
    options.set_preference("http.response.timeout", 5)
    options.set_preference("dom.max_script_run_time", 5)
    options.set_preference("network.proxy_type", 1)
    options.set_preference("network.proxy.http", f"{proxy['IP']}")
    options.set_preference("network.proxy.http_port", f"{proxy['PORT']}")

    driver = Firefox(options=options)
    headers = get_headers()
    # changing default HTTP headers to custom ones with UA rotation
    def interceptor(request):
        for header_name, header_value in headers.items():
            del request.headers[header_name]
            request.headers[header_name] = header_value

    driver.request_interceptor = interceptor
    driver.implicitly_wait(10) # seconds
    # driver.maximize_window()
    driver.get(url)
    return driver


def element_exist(element, driver):
    # class element_has_attribute(object):
    #     def __init__(self, locator):
    #         self.locator = locator

    #     def __call__(self, driver):
    #         element = driver.find_element(*self.locator)   # Finding the referenced element
    #         if element:
    #             return element
    #         else:
    #             return False


    # exist = WebDriverWait(driver, 5).until(
    #     element_has_attribute(element)
    # )
    element = WebDriverWait(driver, 5).until(
        lambda x: x.find_element(element[0], element[1]).get_attribute("src")
        )



def get_breed(a, driver, action):
    url = unquote(a.get_attribute("href"))
    if page_exist(url):
        scroll_shim(driver, a)
        action.move_to_element(a).perform()
        div_popup = driver.find_element(By.CLASS_NAME, "mwe-popups-container")
        # img = WebDriverWait(driver, 5).until(
        #     lambda _: div_popup.find_element(By.CLASS_NAME, "mwe-popups-thumbnail").get_attribute("src")
        #     )
        # img = div_popup.find_element(By.CLASS_NAME, "mwe-popups-thumbnail")
        img = polling2.poll(lambda: driver.find_element(By.CLASS_NAME, "mwe-popups-thumbnail"), step=0.5, timeout=7)
        p = div_popup.find_element(By.TAG_NAME, "p")
        breed = {
            "name": a.text,
            "img": unquote(img.get_attribute("src")),
            "desc": p.text
            }
        return breed


            # img = (By.CLASS_NAME, "mwe-popups-thumbnail")
            # p = (By.TAG_NAME, "p")
            # for element in (img,p):
            #     if element_exist(element, driver):
            #         element = div_popup.find_element(element[0], element[1])
            #         if element.get_attribute("src"):
            #             img = element
            #         else:
            #             p = element

            # breed = {
            #     "name": a.text,
            #     "img": unquote(img.get_attribute("src")),
            #     "desc": p.text
            #     }
            # return breed




            # img = div_popup.find_element(By.CLASS_NAME, "mwe-popups-thumbnail")
            # p = div_popup.find_element(By.TAG_NAME, "p")
            # breed = {
            #     "name": a.text,
            #     "img": unquote(img.get_attribute("src")),
            #     "desc": p.text
            #     }
            # return breed


            # try:
            #     img = div_popup.find_element(By.CLASS_NAME, "mwe-popups-thumbnail")
            #     p = div_popup.find_element(By.TAG_NAME, "p")
            #     print("test-1")
            # except NoSuchElementException as err:
            #     print("test-4")
            #     print(str(err).strip())
            #     return None

            # print("test-2")
            # breed = {
            #     "name": a.text,
            #     "img": unquote(img.get_attribute("src")),
            #     "desc": p.text
            #     }
            # return breed


def get_breeds_from_table(url, animal):
    driver = get_driver(url)
    action = ActionChains(driver)
    xpath = "/html/body/div[3]/div[3]/div[5]/div[1]/table"
    tables = driver.find_elements(By.XPATH, xpath)

    breeds = []
    for table in tables:
        tbody = table.find_element(By.TAG_NAME, "tbody")
        all_a = tbody.find_elements(By.TAG_NAME, "a")
        for a in all_a:
            breed = get_breed(a, driver, action)
            if breed:
                breeds.append(breed)
                print(breed)
    driver.close()
    return breeds


def get_breeds_from_ul(breeds_url):
    driver = get_driver(breeds_url)
    # print(driver.capabilities['browserVersion'])
    action = ActionChains(driver)
    uls = driver.find_elements(By.XPATH, "//div[@class='mw-parser-output']/ul")

    breeds = []
    for ul_tag in uls[:-2]:
        all_a = ul_tag.find_elements(By.TAG_NAME, "a")
        for a in all_a:
            breed = get_breed(a, driver, action)
            if breed:
                breeds.append(breed)
                print(breed)
                print("test-3")
    driver.close()
    return breeds


def get_breeds(url, animal):
    soup = get_soup(url)
    tables = soup.find_all("table", {"class": "wikitable"})

    if tables:
        unfiltered = get_breeds_from_table(url, animal)
    else:
        # return "Pass"
        unfiltered = get_breeds_from_ul(url)

    unique_breeds = list({breed["name"]:breed for breed in unfiltered}.values())
    return unique_breeds


def get_animal(a_tag):
    name = a_tag.text.split()[-1].replace("rassen","")
    url = f"https://de.wikipedia.org{a_tag['href']}"
    # url = "https://de.wikipedia.org/wiki/Liste_von_Gänserassen"
    animal =  {
        "name": name,
        "breeds": get_breeds(url, name),
        }
    print(animal)
    logger.info(f"[INFO] Successfully got {name}")
    return animal


@logger.catch(onerror=lambda _: sys.exit(1))
def main():
    logger.info("[INFO] Start to getting animals data from wikipedia")

    soup = get_soup("https://de.wikipedia.org/wiki/Liste_von_Haustierrassen")
    tr_list = soup.find("table", {"class": "wikitable"}).find("tbody").find_all("tr")
    # TODO почему подставляется breed_url а не breed_list_url???
    # animals = []
    # for tr_tag in tr_list[1:]:
    #     # print(tr_tag.contents[3].text.strip().split(",")[0])
    #     a_tag = tr_tag.contents[3].next
    #     animal = get_animal(a_tag)
    #     animals.append(animal)
    # animals = [get_animal(tr_tag.contents[3].next) for tr_tag in tr_list[1:]]
    animals = [get_animal(tr_tag.contents[3].next) for tr_tag in tr_list[3:]]


    # basedir = Path(__file__).parent
    # basename = "animals.json"
    # with open(f"{basedir}/json/{basename}", "w", encoding="utf-8") as _file:
    #     _file.write(dumps(animals, indent=4, ensure_ascii=False))

    # print(f"[INFO] Successfully wrote all data to '{basedir}/json/{basename}'")


if __name__ == '__main__':
    main()
