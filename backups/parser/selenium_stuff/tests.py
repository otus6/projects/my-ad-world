# pylint: disable=wrong-import-position, import-error
from seleniumwire.webdriver import Firefox
from selenium.webdriver.firefox.options import Options


from seleniumwire.webdriver import Chrome, DesiredCapabilities
# from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.proxy import Proxy, ProxyType
from base import get_proxy, get_headers, make_request
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException
from time import sleep
from urllib.parse import unquote
import re


def get_ff_driver():
    proxy = get_proxy(selenium=True)

    options = Options()
    options.binary_location = "/usr/local/bin/firefox"
    # options.binary_location = "/usr/bin/firefox-esr"

    driver = Firefox(options=options)
    driver.implicitly_wait(5) # seconds
    return driver


def get_chrome_driver():
    options = Options()
    options.headless = True
    options.binary_location = "/usr/bin/google-chrome"

    proxy = get_proxy(selenium=True)
    proxy = Proxy({
        'proxyType': ProxyType.MANUAL,
        'httpProxy': f"{proxy['IP']:{proxy['PORT']}}",
        'noProxy': ''})

    capabilities = DesiredCapabilities.CHROME.copy()
    proxy.add_to_capabilities(capabilities)
    driver = Chrome(options=options, desired_capabilities=capabilities)

    headers = get_headers()
    # changing default HTTP headers to custom ones with UA rotation
    def interceptor(request):
        for header_name, header_value in headers.items():
            del request.headers[header_name]
            request.headers[header_name] = header_value

    driver.request_interceptor = interceptor
    driver.implicitly_wait(5) # seconds
    driver.set_page_load_timeout(5)
    return driver


def scroll_shim(passed_in_driver, tag_object):
    x = tag_object.location["x"]
    y = tag_object.location["y"]
    scroll_by_coord = f"window.scrollTo({x},{y});"
    scroll_nav_out_of_way = "window.scrollBy(0, -120);"
    passed_in_driver.execute_script(scroll_by_coord)
    passed_in_driver.execute_script(scroll_nav_out_of_way)


def match_url(url):
    regex = re.compile(r"https://de.wikipedia.org/wiki/(\w+|(\w+-\w+)+)$")
    match = re.match(regex, url)
    if match:
        return True


def page_exist(url):
    if match_url(url):
        response = make_request(
            url,
            headers=get_headers(),
            proxies=get_proxy()
            )
        if response.status_code == 200:
            return True
    return False


def main():
    driver = get_ff_driver()
    action = ActionChains(driver)
    driver.get("https://de.wikipedia.org/wiki/Liste_von_Gänserassen")
    uls = driver.find_elements(By.XPATH, "//div[@class='mw-parser-output']/ul")

    for ul_tag in uls[:-2]:
        all_a = ul_tag.find_elements(By.TAG_NAME, "a")
        for a in all_a:
            url = unquote(a.get_attribute("href"))
            if page_exist(url):
                scroll_shim(driver, a)
                action.move_to_element(a).perform()
                div_popup = driver.find_element(By.CLASS_NAME, "mwe-popups-container")
                img = div_popup.find_element(By.CLASS_NAME, "mwe-popups-thumbnail")
                sleep(5)

                breed = a.text
                try:
                    # print(img.get_attribute('outerHTML'))
                    img = img.get_attribute('src')
                    print(breed)
                    print(img)
                except StaleElementReferenceException:
                    print(f"broken breed {breed} detected")
                    continue
                # print(unquote(img.get_attribute("src")))


    #     try:
    #         img = div_popup.find_element(By.CLASS_NAME, "mwe-popups-thumbnail")
    #         p = div_popup.find_element(By.TAG_NAME, "p")
    #         print("test-1")
    #     except NoSuchElementException as err:
    #         print("test-4")
    #         print(str(err).strip())
    #         return None

    # print("test-2")
    # breed = {
    #     "name": a.text,
    #     "img": unquote(img.get_attribute("src")),
    #     "desc": p.text
    #     }
    # return breed




if __name__ == '__main__':
    main()
    # try:
    #     driver = get_ff_driver()
    #     # driver = get_chrome_driver()

    #     driver.get("https://de.wikipedia.org/wiki/Wikipedia:Hauptseite")
    #     p = driver.find_element(By.TAG_NAME, "abxfdgd")
    #     print(p.text)
    #     driver.quit()
    # except Exception as err:
    #     driver.quit()
    #     raise err






    # try:
    #     driver = get_ff_driver()
    #     print(driver.binary())
    #     driver.quit()
    # except Exception:
    #     driver.quit()
