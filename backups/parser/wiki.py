# import wikipedia


# wikipedia.set_lang("de")
# page = wikipedia.page("Liste von Haustierrassen")
# # print(dir(page))
# print(page.content)


# import pandas as pd
# import wikipedia as wp

# #Get the html source
# wp.set_lang("de")
# html = wp.page("Liste von Haustierrassen").html().encode("UTF-8")
# df = pd.read_html(html)[0]
# df.to_csv('beautifulsoup_pandas.csv',header=0,index=False)
# print(df)


import requests
from json import dumps

url = "https://de.wikipedia.org/api/rest_v1/page/summary/Aylesburyente"
response = requests.get(url).json()
title = response["title"]
description = response["extract"]

url = "https://de.wikipedia.org/api/rest_v1/page/media-list/Aylesburyente"
response = requests.get(url).json()
images = [src["src"] for item in response["items"] for src in item["srcset"] if src["scale"] == "1x"]

result = {
    "title": title,
    "description": description,
    "images": images
}

print(dumps(result, indent=4, ensure_ascii=False))


test = "https://de.wikipedia.org/wiki/Amerikanische_Pekingente"
print(test.split("/")[-1])