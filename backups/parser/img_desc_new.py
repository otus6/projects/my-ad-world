# pylint: disable=wrong-import-position, import-error, invalid-name
from base import get_headers, get_proxy, make_request
from bs4 import BeautifulSoup
from pathlib import Path
import re
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from seleniumwire.webdriver import Firefox
import sys


def get_html(url):
    response = make_request(
        url,
        headers=get_headers(),
        proxies=get_proxy()
        )
    return response.content


def get_soup(url):
    html = get_html(url)
    soup = BeautifulSoup(html, "html.parser")
    return soup


def match_url(url):
    regex = re.compile(r"https://de.wikipedia.org/wiki/\w+$")
    match = re.match(regex, url)
    if match:
        return True


def page_exist(url):
    if match_url(url):
        response = make_request(
            url,
            headers=get_headers(),
            proxies=get_proxy()
            )
        if response.status_code == 200:
            return True
    return False


def scroll_shim(passed_in_driver, tag_object):
    x = tag_object.location["x"]
    y = tag_object.location["y"]
    scroll_by_coord = f"window.scrollTo({x},{y});"
    scroll_nav_out_of_way = "window.scrollBy(0, -120);"
    passed_in_driver.execute_script(scroll_by_coord)
    passed_in_driver.execute_script(scroll_nav_out_of_way)


def get_driver(url):
    proxy = get_proxy(selenium=True)

    options = Options()
    # options.headless = True
    options.set_preference("http.response.timeout", 5)
    options.set_preference("dom.max_script_run_time", 5)
    options.set_preference("network.proxy_type", 1)
    options.set_preference("network.proxy.http", f"{proxy['IP']}")
    options.set_preference("network.proxy.http_port", f"{proxy['PORT']}")

    driver = Firefox(options=options)
    headers = get_headers()
    # changing default HTTP headers to custom ones with UA rotation
    def interceptor(request):
        for header_name, header_value in headers.items():
            del request.headers[header_name]
            request.headers[header_name] = header_value

    driver.request_interceptor = interceptor
    driver.implicitly_wait(1) # seconds
    # driver.maximize_window()
    driver.get(url)
    return driver


def get_not_breeds(url):
    soup = get_soup(url)
    try:
        uls = soup.find("span", {"id": "Siehe_auch"}).find_all_next("ul")
    except AttributeError:
        uls = soup.find("span", {"id": "Weblinks"}).find_all_next("ul")
    not_breeds = [li.find("a") for ul in uls for li in ul if li.text.strip()]
    return not_breeds


def is_breed(a, url):
    not_breeds = get_not_breeds(url)
    for not_breed in not_breeds:
        if a and a.text != not_breed.text:
            return True


# def get_breeds_from_table(tables, animal):
def get_breeds_from_table(url, animal):
    driver = get_driver(url)
    action = ActionChains(driver)
    tables = driver.find_elements(By.XPATH, "//table[contains(@class, 'wikitable')]/a")
    breeds = []
    for table in tables:
        # WA for cats wasted table (wasted table in the end)
        if table.get_attribute("class") == ["wikitable", "zebra"]:
            continue
        for tr_tag in table.find_elements(By.TAG_NAME, "tr"):
            breed = tr_tag.find_element(By.TAG_NAME, "td")
            # if breed:
            #     if animal.endswith("Ziegen"):
            #         breed = breed.find_next("td")
            #     breed = re.sub(r"\[\d+\]|\(\W\)", "", breed.text.strip())
            #     breeds.append(breed)
    return breeds


    # breeds = []
    # for table in tables:
    #     # WA for cats wasted table (wasted table in the end)
    #     if table["class"] == ['wikitable', 'zebra']:
    #         continue
    #     for tr_tag in table.find_all("tr"):
    #         breed = tr_tag.find("td")
    #         if breed:
    #             if animal.endswith("Ziegen"):
    #                 breed = breed.find_next("td")
    #             breed = re.sub(r"\[\d+\]|\(\W\)", "", breed.text.strip())
    #             breeds.append(breed)
    # return breeds


def get_breeds_from_ul(breeds_url):
    driver = get_driver(breeds_url)
    action = ActionChains(driver)
    uls = driver.find_elements(By.TAG_NAME, "ul")
    breeds = []
    for ul_tag in uls:
        all_a = ul_tag.find_elements(By.TAG_NAME, "a")
        for a in all_a:
            url = a.get_attribute("href")
            if page_exist(url) and is_breed(a, breeds_url):
            # if page_exist(url):
                scroll_shim(driver, a)
                action.move_to_element(a).perform()
                div_popup = driver.find_element(By.CLASS_NAME, "mwe-popups-container")
                if div_popup:
                    img = div_popup.find_element(By.CLASS_NAME, "mwe-popups-thumbnail")
                    p = div_popup.find_element(By.TAG_NAME, "p")
                    breed = {
                        "name": a.text,
                        "img": img.get_attribute("src"),
                        "desc": p.text
                    }
                    breeds.append(breed)
                    print(breed)
    driver.close()
    return breeds


def delete_wasted_raws(animal, breeds):
    others = ("Schaf", "Schweine", "Haustauben", "Puten", "Gänse")
    if animal == "Enten":
        return breeds[:-4]
    if animal == "Rinder":
        return breeds[:-3]
    if animal in others:
        return breeds[:-1]
    return breeds


def get_breeds(url, animal):
    soup = get_soup(url)
    tables = soup.find_all("table", {"class": "wikitable"})

    if tables:
        unfiltered = get_breeds_from_table(url, animal)
    else:
        unfiltered = get_breeds_from_ul(url)

    not_breeds = get_not_breeds(url)
    breeds = list((filter(lambda breed: (breed in not_breed for not_breed in not_breeds), unfiltered)))
    unique_breeds = list({breed:breed for breed in breeds}.values())
    breeds = delete_wasted_raws(animal, unique_breeds)
    return breeds


# def get_animal(a_tag, animals):
def get_animal(a_tag):
    name = a_tag.text.split()[-1].replace("rassen","")
    url = f"https://de.wikipedia.org{a_tag['href']}"
    print(url)
    animal =  {
        "name": name,
        "breeds": get_breeds(url, name),
        }
    print(f"[INFO] Successfully got {name}")
    return animal


def main():
    print("[INFO] Start to getting animals data from wikipedia")

    soup = get_soup("https://de.wikipedia.org/wiki/Liste_von_Haustierrassen")
    tr_list = soup.find("table", {"class": "wikitable"}).find("tbody").find_all("tr")
    # TODO почему подставляется breed_url а не breed_list_url???
    # animals = []
    # for tr_tag in tr_list[1:]:
    #     # print(tr_tag.contents[3].text.strip().split(",")[0])
    #     a_tag = tr_tag.contents[3].next
    #     animal = get_animal(a_tag)
    #     animals.append(animal)
    animals = [get_animal(tr_tag.contents[3].next) for tr_tag in tr_list[1:]]


    # basedir = Path(__file__).parent
    # basename = "animals.json"
    # with open(f"{basedir}/json/{basename}", "w", encoding="utf-8") as _file:
    #     _file.write(dumps(animals, indent=4, ensure_ascii=False))

    # print(f"[INFO] Successfully wrote all data to '{basedir}/json/{basename}'")


if __name__ == '__main__':
    main()
