# pylint: disable=wrong-import-position
from bs4 import BeautifulSoup
import random
import requests
import sys


def make_request(URL, timeout=5, headers=None, proxies=None):
    try:
        reply = requests.get(URL, timeout=timeout, headers=headers, proxies=proxies)
    except requests.exceptions.Timeout as err:
        print(f"[ERROR] Timeout {timeout} seconds has expired. More info: {err}")
        sys.exit(1)
    except(requests.RequestException, ValueError) as err:
        print(f"[ERROR] Oops some error occured. More info: {err}")
        sys.exit(1)
    return reply


def get_proxy(selenium=None):
    reply = make_request("https://free-proxy-list.net/")
    soup_fpl = BeautifulSoup(reply.content, "html.parser")

    table = soup_fpl.find("div", {"class": "table-responsive"}).find("table", {"class": "table table-striped table-bordered"})
    proxies = []
    for tr in table.find("tbody").find_all("tr"):
        td_list = tr.find_all("td")
        proxy_item = {
            "IP": str(td_list[0].text),
            "PORT": str(td_list[1].text),
            "CODE": str(td_list[2].text),
            "COUNTRY": str(td_list[3].text),
            "ANONYMITI": str(td_list[4].text),
            "GOOGLE": str(td_list[5].text),
            "HTTPS": str(td_list[6].text),
            "LAST_CHECK": str(td_list[7].text)
        }
        # if proxy_item["ANONYMITI"].startswith("elite") and proxy_item["HTTPS"] == "yes":
        if proxy_item["ANONYMITI"].startswith("elite"):
            if selenium:
                proxies.append(proxy_item)
            else:
                proxy = f"http://{proxy_item['IP']}:{proxy_item['PORT']}"
                proxies.append({
                    "http": proxy,
                    # "https": proxy
                })
    return random.choice(proxies)


def get_headers():
    user_agent_list = [
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.0.0 Safari/537.36",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15",
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:77.0) Gecko/20100101 Firefox/77.0",
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36",
    ]

    #Pick a random user agent
    user_agent = random.choice(user_agent_list)

    #Set the headers
    headers = {
        "User-Agent": user_agent,
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
        "Accept-Language": "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3",
        "Referer": "https://www.google.com/",
        # "Accept-Encoding": "gzip, deflate, br",
        "DNT": "1",
        "Connection": "keep-alive",
        # Requests sorts cookies= alphabetically
        # "Cookie": "route_97385225_bd99_4ddb_a1c6_c108f3714d67=d3d99b3d7a1aebd181b8271f5243ba76; CSRF-TOKEN=fbe80a72-c73a-408b-8c5f-7775d79556f2; up=%7B%22ln%22%3A%2251380498%22%2C%22lln%22%3A%224841d292-ed70-42b6-975c-8351faa78f1d%22%2C%22llstv%22%3A%2297-js-errorlog%3DA%7CBLN-18532_highlight%3DB%7CBLN-20955_Buy_now_kill%3DB%7CBLN-18275-lazy-load-image%3DB%7CEBAYKAD-3536_floor_ai%3DB%7CDesktop_Test%3DA%7Cpromotion-desktop%3DA%7CEKMO-25_MyAdsC2b%3DB%7Cperformance-test-desktop%3DA%7CEKMO-11%3DB%7Cga-behind-consent%3DB%7Crebranding-m2www%3DA%7Cconsent-banner%3DA%7Cdesktop_decision_engine%3DA%7CBLN-18221_srp_rebrush_ads%3DB%7CBLN-19787-biz-upselling%3DB%7Cprebid-update%3DA%7CBLN-21421_badge_srp%3DB%7Cliberty_gcp_desktop%3DA%7CEBAYKAD-2252_group-assign%3DB%22%2C%22ls%22%3A%22*%22%7D; GCLB=CLX5pP7MgOyVAQ; clientId=undefined; ekConsentTcf2={%22customVersion%22:3%2C%22encodedConsentString%22:%22CPcLWBmPcLWBmE1ABADECQCgAP_AAAAAAAYgIxNd_X__bX9n-_7_7ft0eY1f9_r3_-QzjhfNs-8F3L_W_L0X32E7NF36tq4KuR4ku3bBIQNtHMnUTUmxaolVrzHsak2cpyNKJ7LkknsZe2dYGH9Pn9lD-YKZ7_5___f53T___9_-39z3_9f___d__-__-vjf_599n_v9fV_7___________-_________wAAAEhoAMAAQRiCQAYAAgjEUgAwABBGIlABgACCMRCADAAEEYh0AGAAIIxDIAMAAQRiFQAYAAgjEIgAwABBGIA.YAAAAAAAAyoA%22%2C%22googleConsentGiven%22:true%2C%22consentInterpretation%22:{%22googleAdvertisingFeaturesAllowed%22:true%2C%22googleAnalyticsAllowed%22:true%2C%22infonlineAllowed%22:true%2C%22theAdexAllowed%22:true}}; ekConsentBucketTcf2=full2-exp; liberty=%7B%22gdprPropertiesForUser%22%3A%7B%22isGdprFullConsentGiven%22%3Atrue%7D%7D",
        "Upgrade-Insecure-Requests": "1",
        "Sec-Fetch-Dest": "document",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-Site": "cross-site",
        # Requests doesn"t support trailers
        # "TE": "trailers",
        }
    return headers
