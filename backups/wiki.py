# pylint: disable=wrong-import-position, import-error
from base import get_headers, get_proxy, make_request
from bs4 import BeautifulSoup
from json import dumps
import re


def get_html(URL):
    response = make_request(
        URL,
        headers=get_headers(),
        proxies=get_proxy()
        )
    return response.content


def get_soup(URL):
    html = get_html(URL)
    soup = BeautifulSoup(html, "html.parser")
    return soup


def get_breeds(url):
    print(url)
    soup = get_soup(url)
    regex = re.compile(r"\w{1}")
    spans = soup.find("div", {"class": "mw-parser-output"}).find_all("span", {"id": regex})

    breeds = []
    firstspan = spans[0]
    next_ul = firstspan.find_next('ul')
    def loop_until_a(text, first_element):
        for a_tag in next_ul.find_all("a"):
            url = f"https://de.wikipedia.org{a_tag['href']}"
            reply = make_request(url)
            if reply.status_code == 200:
                soup = get_soup(url)
                description = soup.find("p").text.strip()
                photo = soup.find("img")
                if description.endswith("lade bitte die Seite neu."):
                    continue
                breed = {
                    "name": a_tag.text,
                    "description": re.sub(r"\[\d\]", "", description),
                    "photo": photo["src"],
                }
                breeds.append(breed)



        text += first_element.string
        if (first_element.next.next == next_ul):
            return text
            #Using double next to skip the string nodes themselves
        return loop_until_a(text, first_element.next.next)

    target_string = loop_until_a('', firstspan)
    print(target_string)





def get_animal(a_tag):
    name = a_tag.text.split()[-1].replace("rassen","")
    url = f"https://de.wikipedia.org{a_tag['href']}"
    animal =  {
        "name": name,
        "breeds": get_breeds(url),
        }
    return animal


def main():
    soup = get_soup("https://de.wikipedia.org/wiki/Liste_von_Haustierrassen")
    tr_list = soup.find("table", {"class": "wikitable"}).find("tbody").find_all("tr")
    animals = [
        get_animal(a) for tr in tr_list for a in tr.find_all("a") if a.text.startswith("Liste")
        ]
    # print(dumps(animals, indent=4, ensure_ascii=False))

if __name__ == '__main__':
    main()
    