# pylint: disable=wrong-import-position, import-error
from base import get_headers, get_proxy, make_request
from bs4 import BeautifulSoup
from json import dumps
import re


def get_html(URL):
    response = make_request(
        URL,
        headers=get_headers(),
        proxies=get_proxy()
        )
    return response.content


def get_soup(URL):
    html = get_html(URL)
    soup = BeautifulSoup(html, "html.parser")
    return soup


def get_not_breeds(URL):
    soup = get_soup(URL)
    uls = soup.find("span", {"id": "Siehe_auch"}).find_all_next("ul")
    not_breeds = [li.text.strip() for ul in uls for li in ul if li.text.strip()]
    return not_breeds


def get_breed_data(URL, breed_name):
    # написать парсер для страниц с таблицами
    reply = make_request(URL)
    if reply.status_code == 200:
        soup = get_soup(URL)
        description = soup.find("p").text.strip()
        photo = soup.find("img")
        if description.endswith(
            "lade bitte die Seite neu."
            ) or description.startswith(
                "Wikipedia ist ein Projekt zum Aufbau"):
            return False
        breed = {
            "name": breed_name,
            "description": re.sub(r"\[\d\]", "", description),
            "photo": photo["src"],
        }
        # print(breed)
        return breed

# fake.cats_breed()
# fake.cats_desc()
# fake.cats_photo()

def get_breeds(breed_list_url):
    soup = get_soup(breed_list_url)
    uls = soup.find("div", {"class": "mw-parser-output"}).find_all("ul")
    unfiltered = []
    for ul in uls:
        for li_tag in ul:
            if li_tag.text.strip():
                a_tag = li_tag.find("a")
                try:
                    breed_name = a_tag.text
                except AttributeError:
                    continue
                if "http://" in a_tag["href"]:
                    break
                breed_url = f"https://de.wikipedia.org{a_tag['href']}"
                breed = get_breed_data(breed_url, breed_name)
                if not breed:
                    continue
                print(breed)
                unfiltered.append(breed)

    not_breeds = get_not_breeds(breed_list_url)
    breeds = list((filter(lambda breed: breed["name"] not in not_breeds, unfiltered)))
    unique_breeds = list({breed["name"]:breed for breed in breeds}.values())
    return unique_breeds


def get_animal(a_tag):
    name = a_tag.text.split()[-1].replace("rassen","")
    url = f"https://de.wikipedia.org{a_tag['href']}"
    animal =  {
        "name": name,
        "breeds": get_breeds(url),
        }
    return animal


def main():
    soup = get_soup("https://de.wikipedia.org/wiki/Liste_von_Haustierrassen")
    tr_list = soup.find("table", {"class": "wikitable"}).find("tbody").find_all("tr")
    animals = [
        get_animal(a) for tr in tr_list for a in tr.find_all("a") if a.text.startswith("Liste")
        ]
    # print(dumps(animals, indent=4, ensure_ascii=False))

if __name__ == '__main__':
    main()
    # get_filter()
