# pylint: disable=wrong-import-position, import-error
from base import get_headers, get_proxy, make_request
from bs4 import BeautifulSoup
from contextlib import closing
from json import dumps
from pathlib import Path
import re
from selenium.common.exceptions import MoveTargetOutOfBoundsException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from seleniumwire.webdriver import Firefox
import sys
from time import sleep


def get_html(URL):
    response = make_request(
        URL,
        headers=get_headers(),
        proxies=get_proxy()
        )
    return response.content


def get_soup(URL):
    html = get_html(URL)
    soup = BeautifulSoup(html, "html.parser")
    return soup


def scroll_shim(passed_in_driver, object):
    x = object.location['x']
    y = object.location['y']
    scroll_by_coord = 'window.scrollTo(%s,%s);' % (
        x,
        y
    )
    scroll_nav_out_of_way = 'window.scrollBy(0, -120);'
    passed_in_driver.execute_script(scroll_by_coord)
    passed_in_driver.execute_script(scroll_nav_out_of_way)


def get_html_selenium(url):
    proxy = get_proxy(selenium=True)

    options = Options()
    options.set_preference("http.response.timeout", 5)
    options.set_preference("dom.max_script_run_time", 5)
    options.set_preference("network.proxy_type", 1)
    options.set_preference("network.proxy.http", f"{proxy['IP']}")
    options.set_preference("network.proxy.http_port", f"{proxy['PORT']}")

    with closing(Firefox(options=options)) as driver:
        headers = get_headers()
        # changing default HTTP headers to custom ones with UA rotation
        def interceptor(request):
            for header_name, header_value in headers.items():
                del request.headers[header_name]
                request.headers[header_name] = header_value

        driver.request_interceptor = interceptor
        driver.implicitly_wait(1) # seconds
        driver.maximize_window()
        driver.get(url)

        # a class="mw-redirect"
        # div class="mwe-popups-container" ->
        # img["src"] class="mwe-popups-thumbnail"
        # <p>

        action = ActionChains(driver)
        uls = driver.find_elements(By.TAG_NAME, "ul")
        for ul_tag in uls:
            all_a = ul_tag.find_elements(By.TAG_NAME, "a")
            for a in all_a:
                if a:
                    if a.get_attribute("class") == "mw-redirect":
                        scroll_shim(driver, a)
                        action.move_to_element(a).perform()
                        div_popup = driver.find_element(By.CLASS_NAME, "mwe-popups-container")
                        if div_popup:
                            img = div_popup.find_element(By.CLASS_NAME, "mwe-popups-thumbnail")
                            p = div_popup.find_element(By.TAG_NAME, "p")
                            print(a.text)
                            print(img.get_attribute("src"))
                            print(p.text)



                        # try:
                        #     action.move_to_element(a).perform()
                        #     div_popup = driver.find_element(By.CLASS_NAME, "mwe-popups-container")
                        #     if div_popup:
                        #         img = div_popup.find_element(By.CLASS_NAME, "mwe-popups-thumbnail")
                        #         p = div_popup.find_element(By.TAG_NAME, "p")
                        #         print(img.get_attribute("src"))
                        #         print(p.text)
                        # except MoveTargetOutOfBoundsException:
                        #     driver.execute_script("arguments[0].scrollIntoView(true);", a)

            # action.move_to_element(a_tag).perform()
            # div_popup = driver.find_element(By.CLASS_NAME, "mwe-popups-container")
            # if div_popup:
            #     img = driver.find_element(By.CLASS_NAME, "mwe-popups-thumbnail")
            #     print(img["src"])


        # driver.execute_script("arguments[0].click()", cookie_btn)
        # sleep(3)

        # button = driver.find_element(By.ID, "srch-ctgry-inpt")
        # button.click()
        # sleep(3)
        # return driver.page_source


def get_not_breeds(URL):
    soup = get_soup(URL)
    try:
        uls = soup.find("span", {"id": "Siehe_auch"}).find_all_next("ul")
    except AttributeError:
        uls = soup.find("span", {"id": "Weblinks"}).find_all_next("ul")
    not_breeds = [li.text.strip() for ul in uls for li in ul if li.text.strip()]
    return not_breeds


def get_breeds_from_table(tables, animal):
    breeds = []
    for table in tables:
        # WA for cats wasted table (wasted table in the end)
        if table["class"] == ['wikitable', 'zebra']:
            continue
        for tr_tag in table.find_all("tr"):
            breed = tr_tag.find("td")
            if breed:
                if animal.endswith("Ziegen"):
                    breed = breed.find_next("td")
                breed = re.sub(r"\[\d+\]|\(\W\)", "", breed.text.strip())
                breeds.append(breed)
    return breeds


def get_breeds_from_ul(uls, url):
    get_html_selenium(url)
    sys.exit(0)

    breeds = []
    for ul_tag in uls:
        for li_tag in ul_tag:
            if li_tag.text.strip():
                try:
                    li_tag["class"]
                except KeyError:
                    a_tag = li_tag.find("a")
                    try:
                        breed = a_tag.text
                    except AttributeError:
                        continue
                    # TODO сходить selenium на общую страничку (наверное можно до цикла)
                    # TODO надо один раз зайти на страницу, сделать там mouse over на нужный <a>, сделать if a, 
                    # TODO взять img["href"] и <p> у нужных <div> и повторить это для всех
                    # TODO понадобиться url из get_breeds()
                    if "http://" in a_tag["href"] or "Entente" in breed:
                        break
                    breed = re.sub(r"\[\d+\]|\[FAO\s\d+\]", "", breed.strip())
                    if breed:
                        breeds.append(breed)
                else:
                    # WA for cows (wasted ul in the beginning)
                    regex = re.compile(r"toc\w+\-\d+")
                    if all(regex.match(item) for item in li_tag["class"]):
                        continue
    return breeds


def delete_wasted_raws(animal, breeds):
    others = ("Schaf", "Schweine", "Haustauben", "Puten", "Gänse")
    if animal == "Enten":
        return breeds[:-4]
    if animal == "Rinder":
        return breeds[:-3]
    if animal in others:
        return breeds[:-1]
    return breeds


def get_breeds(url, animal):
    soup = get_soup(url)
    uls = soup.find("div", {"class": "mw-parser-output"}).find_all("ul")
    tables = soup.find_all("table", {"class": "wikitable"})

    unfiltered = []
    if tables:
        breeds = get_breeds_from_table(tables, animal)
        unfiltered += breeds
    else:
        breeds = get_breeds_from_ul(uls, url)
        unfiltered += breeds

    not_breeds = get_not_breeds(url)
    breeds = list((filter(lambda breed: (breed in not_breed for not_breed in not_breeds), unfiltered)))
    unique_breeds = list({breed:breed for breed in breeds}.values())
    breeds = delete_wasted_raws(animal, unique_breeds)
    return breeds


def get_animal(a_tag, animals):
    name = a_tag.text.split()[-1].replace("rassen","")
    url = f"https://de.wikipedia.org{a_tag['href']}"
    dogs = ("FCI-Hunde", "(JGHV)")
    if name in dogs:
        name = "Haushunde"
        for animal in animals:
            if animal["name"] == name:
                animal["breeds"] += get_breeds(url, name)
                return

    animal =  {
        "name": name,
        "breeds": get_breeds(url, name),
        }
    print(f"[INFO] Successfully got {name}")
    return animal


def main():
    print("[INFO] Start to getting animals data from wikipedia")

    soup = get_soup("https://de.wikipedia.org/wiki/Liste_von_Haustierrassen")
    tr_list = soup.find("table", {"class": "wikitable"}).find("tbody").find_all("tr")
    dogs = ("Liste der FCI-Hunderassen", "Liste der Jagdhundrassen (JGHV)")
    animals = []
    for tr_tag in tr_list:
        for a_tag in tr_tag.find_all("a"):
            if a_tag.text.startswith("Liste") and a_tag.text != "Liste seltener Hunderassen":
                if a_tag.text in dogs:
                    get_animal(a_tag, animals)
                    continue
                animals.append(get_animal(a_tag, animals))

    # basedir = Path(__file__).parent
    # basename = "animals.json"
    # with open(f"{basedir}/json/{basename}", "w", encoding="utf-8") as _file:
    #     _file.write(dumps(animals, indent=4, ensure_ascii=False))

    # print(f"[INFO] Successfully wrote all data to '{basedir}/json/{basename}'")


if __name__ == '__main__':
    main()
