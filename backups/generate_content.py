# pylint: disable=no-member, wrong-import-position, duplicate-code
import sys
sys.path.append("/home/geezy/Projects/Flask/my-ad-world")
from app import create_app
from app.logger.base import get_app_mode
from app.models.base import db
from app.models import Category, Subcategory
import argparse
from json import dumps, load
from faker import Faker




def init_parser() -> argparse.Namespace:
    """It initializes argument parser for module.

    Returns:
        Namespace: Passed arguments to the module.
    """
    parser = argparse.ArgumentParser(
        description="This module generate content for this flask application."
    )
    parser.add_argument(
        "-d",
        "--dev",
        help=(
            "With this option tool write all generated content to database for development environment. "
            "WARNING! By default (without this key) it works in production mode."
            ),
        action="store_true",
    )
    parser.add_argument(
        "-t",
        "--testing",
        help=(
            "With this option tool write all generated content to database for testing environment. "
            "WARNING! By default (without this key) it works in production mode."
            ),
        action="store_true",
    )
    args = parser.parse_args()

    return args


def get_categories():
    categories = [
        {
            "Pets": [
                "Cats", 
                "Dogs", 
                "Small animals", 
                "Birds"
                ]
        },
        {
            "For Pets": [
                "Homes", 
                "Feed", 
                "Accessories", 
                "Bioadditives and medicines", 
                "Toys", 
                "Clothes"
                ]
        },
        {
            "Give away and exchange": [
                "Give away", 
                "Exchange"
                ],
        },
        {
            "Journey": [
                "Ammunition", 
                "Accessories", 
                "Bags", 
                "Glasses"
            ]
        },
        {
            "Food": [
                "Health", 
                "Vegan", 
                "Meat", 
                "Water and drinks", 
                "Bakery"
            ]
        }
    ]
    return categories


def write_categories(app, categories):
    with app.app_context():
        for dict in categories:
            for category, subcategories in dict.items():
                category = Category(
                    name=category
                )
                db.session.add(category)                    # pylint: disable=no-member
                for subcategory in subcategories:
                    subcategory = Subcategory(
                        name=subcategory,
                        parent=category
                    )
                    db.session.add(subcategory)             # pylint: disable=no-member
                db.session.commit()                         # pylint: disable=no-member


def main() -> None:
    """Main function, that runs this module.
    """
    args = init_parser()
    app_mode = "production"
    if args.dev:
        app_mode = "development"
    elif args.testing:
        app_mode = "testing"
    get_app_mode(app_mode)
    app = create_app(app_mode)
    
    categories = get_categories()
    write_categories(app, categories)


    sys.exit(0)




    fake = Faker("de_DE")
    print(fake.text())
    sys.exit(0)



if __name__ == '__main__':
    # categories = get_categories()
    # with open("json/categories.json", "w") as _file:
    #     _file.write(json.dumps(categories, indent=4))
    main()
