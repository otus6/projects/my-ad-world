# pylint: disable=wrong-import-position, import-error
from typing import Optional
from base import get_headers, get_proxy, make_request, logger
from bs4 import BeautifulSoup, element
from json import dumps
from random import random
import re
import requests
from pathlib import Path
import sys
from tqdm.auto import tqdm
from tqdm.contrib.logging import logging_redirect_tqdm
from urllib.parse import unquote


def get_html(url: str) -> requests.Response:
    """Returns html content of website.

    Args:
        url (str): Website address.

    Returns:
        requests.Response: Html content.
    """
    response = make_request(
        url,
        headers=get_headers(),
        proxies=get_proxy()
        )
    return response.content


def get_soup(url: str) -> BeautifulSoup:
    """Returns BeautifulSoup object of website.

    Args:
        url (str): Website address.

    Returns:
        BeautifulSoup: BeautifulSoup object.
    """
    html = get_html(url)
    soup = BeautifulSoup(html, "html.parser")
    return soup


def match_url(url: str) -> Optional[bool]:
    """Matches URL to regex.

    Args:
        url (str): Website address.

    Returns:
        (Optional[bool], optional): True if URL was matched and False if not.
    """
    regex = re.compile(r"https://de.wikipedia.org/wiki/(\w+|(\w+-\w+)+)$")
    match = re.match(regex, url)
    if match:
        return True
    return False


def page_exist(url: str) -> bool:               # pylint: disable=inconsistent-return-statements
    """Checks if page exist.

    Args:
        url (str): Website address.

    Returns:
        bool: True if page returns HTTP code 200.
    """
    if match_url(url):
        response = make_request(
            url,
            headers=get_headers(),
            proxies=get_proxy()
            )
        if response.status_code == 200:
            return True
        return False
    return False


def get_breed(a_tag: element.Tag, animal_type: str) -> Optional[dict]:
    """Returns breed dictionary or None.

    Args:
        a_tag (element.Tag): <a> html tag with href.
        animal_type (str): English name of animal type.

    Returns:
        Optional[dict]: Breed dictionary or None.
    """
    search_page = unquote(a_tag['href'])
    if search_page.endswith("Liste_von_Haustierrassen"):
        return None
    url = f"https://de.wikipedia.org{search_page}"
    if page_exist(url):
        search_page = url.split("/")[-1]
        summary_url = f"https://de.wikipedia.org/api/rest_v1/page/summary/{search_page}"
        media_url = f"https://de.wikipedia.org/api/rest_v1/page/media-list/{search_page}"
        breed = {}
        for idx, url in enumerate((summary_url, media_url)):
            response = make_request(
                url,
                headers=get_headers(),
                proxies=get_proxy()
            ).json()
            if idx == 0:
                if "sprache" in response["title"].lower():
                    return None
                breed["name"] = response["title"]
                breed["description"] = response["extract"]
            elif idx == 1:
                try:
                    images = [
                         f"https:{src['src']}" for item in response["items"] for src in item["srcset"] if src["scale"] == "1x"  # pylint: disable=line-too-long
                        ]
                except KeyError:
                    return None
                if not images:
                    return None
                breed["images"] = images
                breed["type"] = get_type(animal_type)
        return breed
    return None


def get_breeds_from_table(tables: list, name: str) -> list:
    """Returns animal breeds from html tags <table> on page.

    Args:
        tables (list): Bs4.element.Tag's list of html tables.
        name (str): Animal name (type).

    Returns:
        list: List of animal breeds.
    """
    breeds = []
    for table in tables:
        all_a = table.find("tbody").find_all("a", href=True)
        if name == "Katzen":
            all_a = sorted(all_a[6:], key=lambda k: random())[:5]
        elif name == "Pferde":
            all_a = sorted(all_a, key=lambda k: random())[:1]
        for a_tag in all_a:
            breed = get_breed(a_tag, name)
            if breed:
                breeds.append(breed)
                tqdm.write(dumps(breed, indent=4, ensure_ascii=False))
    return breeds


def get_breeds_from_ul(soup: BeautifulSoup, name: str) -> list:
    """Returns animal breeds from html tags <ul> on page.

    Args:
        soup (BeautifulSoup): BeautifulSoup object.
        name (str): Animal name (type).

    Returns:
        list: List of animal breeds.
    """
    uls = soup.find("div", {"id": "content"}).find_all("ul")

    breeds = []
    for ul_tag in uls[:-3]:
        all_a = ul_tag.find_all("a", href=True)
        reduce_list = (
            "Haushunde",
            "Rinder",
            "Schaf",
            "Schweine",
            "Haustauben"
            )
        if name in reduce_list:  # to reduce count of animals breeds
            all_a = all_a[:1]
        for a_tag in all_a:
            breed = get_breed(a_tag, name)
            if breed:
                breeds.append(breed)
                tqdm.write(dumps(breed, indent=4, ensure_ascii=False))
    return breeds


def get_breeds(url: str, name: str) -> list:
    """Returns all unique breeds found on wiki.

    Args:
        url (str): Website address.
        name (str): Animal name (type).

    Returns:
        list: List of all animal breeds.
    """
    soup = get_soup(url)
    tables = soup.find("div", {"id": "bodyContent"}).find_all("table", {"class": "wikitable"})

    if tables:
        unfiltered = get_breeds_from_table(tables, name)
    else:
        unfiltered = get_breeds_from_ul(soup, name)

    unique_breeds = list({breed["name"]:breed for breed in unfiltered if breed}.values())
    return unique_breeds


def get_type(name: str) -> str:
    """Convert german animal name to english one to get "type" key in dictionary.

    Args:
        name (str): German animal name.

    Returns:
        str: English animal name.
    """
    convert_dict = {
        "Katzen": "Cats",
        "Enten": "Ducks",
        "Haushunde": "Dogs",
        "Esel": "Donkeys",
        "Gänse": "Gooses",
        "Pferde": "Horses",
        "Rinder": "Cows",
        "Schaf": "Pigs",
        "Schweine": "Pigs",
        "Haustauben": "Pigeons",
        "Puten": "Turkeys",
    }
    return convert_dict[name]


def get_animal(a_tag: element.Tag) -> dict:
    """Returns animal dictionary parsed from wikipedia.

    Args:
        a_tag (element.Tag): <a> html tag with href.

    Returns:
        dict: Animal dictionary
    """
    name = a_tag.text.split()[-1].replace("rassen","")
    search_page = unquote(a_tag['href'])
    url = f"https://de.wikipedia.org{search_page}"
    animal =  {
        "name": name,
        "breeds": get_breeds(url, name),
        "type": get_type(name)
        }
    tqdm.write(str(animal))
    with logging_redirect_tqdm():
        logger.info(f"[INFO] Successfully got {name}")
    return animal


@logger.catch(onerror=lambda _: sys.exit(1))
def main() -> None:
    """Main tool function
    """
    with logging_redirect_tqdm():
        logger.info("[INFO] Start to getting animals data from wikipedia")

    soup = get_soup("https://de.wikipedia.org/wiki/Liste_von_Haustierrassen")
    tr_list = soup.find("table", {"class": "wikitable"}).find("tbody").find_all("tr")
    drop_list = (
        "Liste der FCI-Hunderassen",
        "Liste der Jagdhundrassen (JGHV)",
        "Liste seltener Hunderassen",
        "Liste von Hühnerrassen",
        "Kaninchenrassen",
        "Meerschweinchenrassen",
        "Yak-Rassen",
        "Liste von Ziegenrassen",
        "Rassen der Westlichen Honigbiene"
        )
    desc = "Parsing progress"
    animals = [
        get_animal(tr_tag.contents[3].next) for tr_tag in tqdm(tr_list[1:], desc=desc, position=1) if tr_tag.contents[3].next.text not in drop_list # pylint: disable=line-too-long
        ]

    basedir = Path(__file__).parent
    basename = "animals.json"
    with open(f"{basedir}/json/{basename}", "w", encoding="utf-8") as _file:
        _file.write(dumps(animals, indent=4, ensure_ascii=False))

    with logging_redirect_tqdm():
        logger.info(f"[INFO] Successfully wrote all data to '{basedir}/json/{basename}'")


if __name__ == '__main__':
    main()
