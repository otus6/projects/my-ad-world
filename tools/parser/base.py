# pylint: disable=wrong-import-position, import-error
from bs4 import BeautifulSoup
import random
import requests
from typing import Optional

from pathlib import Path
import sys
projectdir = Path(__file__).parents[2]
sys.path.append(str(projectdir))
from tools.logger import logger


basename = Path(__file__).parent.name
basedir = Path(__file__).parent.parent
logger.add(f"{basedir}/logs/{basename}.log", rotation="10 MB")


def make_request(
    url: str,
    timeout: Optional[int] = 5,
    headers: Optional[dict] = None,
    proxies: Optional[dict] = None
    ) -> requests.Response:
    """Makes HTTP request to URL.

    Args:
        url (str): URL to request.
        timeout (Optional[int], optional): Timeout for response awaiting. Defaults to 5.
        headers (Optional[dict], optional): Preconfigured HTTP headers. Defaults to None.
        proxies (Optional[dict], optional): Proxy settings. Defaults to None.

    Returns:
        Response: Reply from website.
    """
    try:
        reply = requests.get(url, timeout=timeout, headers=headers, proxies=proxies)
    except requests.exceptions.Timeout as err:
        logger.error(f"[ERROR] Timeout {timeout} seconds has expired. More info: {err}")
        sys.exit(1)
    except(requests.RequestException, ValueError) as err:
        logger.error(f"[ERROR] Oops some error occured. More info: {err}")
        sys.exit(1)
    return reply


def get_proxy(selenium: Optional[bool] = None) -> dict:
    """Returns random proxy from free-proxy-list.net. Now works only in HTTP mode.

    Args:
        selenium (Optional[bool], optional): Switches to selenium proxy format. Defaults to None.

    Returns:
        dict: Proxy settings.
    """
    reply = make_request("https://free-proxy-list.net/")
    soup_fpl = BeautifulSoup(reply.content, "html.parser")

    table = soup_fpl.find(
        "div", {"class": "table-responsive"}).find(
            "table", {"class": "table table-striped table-bordered"}
            )
    proxies = []
    for tr_tag in table.find("tbody").find_all("tr"):
        td_list = tr_tag.find_all("td")
        proxy_item = {
            "IP": str(td_list[0].text),
            "PORT": str(td_list[1].text),
            "CODE": str(td_list[2].text),
            "COUNTRY": str(td_list[3].text),
            "ANONYMITI": str(td_list[4].text),
            "GOOGLE": str(td_list[5].text),
            "HTTPS": str(td_list[6].text),
            "LAST_CHECK": str(td_list[7].text)
        }
        # if proxy_item["ANONYMITI"].startswith("elite") and proxy_item["HTTPS"] == "yes":
        if proxy_item["ANONYMITI"].startswith("elite"):
            if selenium:
                proxies.append(proxy_item)
            else:
                proxy = f"http://{proxy_item['IP']}:{proxy_item['PORT']}"
                proxies.append({
                    "http": proxy,
                    # "https": proxy
                })
    return random.choice(proxies)


def get_headers() -> dict:
    """Returns HTTP headers configuration.

    Returns:
        dict: HTTP headers configuration.
    """
    user_agent_list = [
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:102.0) Gecko/20100101 Firefox/102.0",
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) \
            Chrome/103.0.0.0 Safari/537.36",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/605.1.15 (KHTML, like Gecko)\
             Version/13.1.1 Safari/605.1.15",
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) \
            Chrome/83.0.4103.97 Safari/537.36",
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:77.0) Gecko/20100101 Firefox/77.0",
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) \
            Chrome/83.0.4103.97 Safari/537.36",
    ]

    #Pick a random user agent
    user_agent = random.choice(user_agent_list)

    #Set the headers
    headers = {
        "User-Agent": user_agent,
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,\
            */*;q=0.8",
        "Accept-Language": "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3",
        "Referer": "https://www.google.com/",
        # "Accept-Encoding": "gzip, deflate, br",
        "DNT": "1",
        "Connection": "keep-alive",
        # Requests sorts cookies= alphabetically
        "Upgrade-Insecure-Requests": "1",
        "Sec-Fetch-Dest": "document",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-Site": "cross-site",
        # Requests doesn"t support trailers
        # "TE": "trailers",
        }
    return headers
