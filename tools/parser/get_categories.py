# pylint: disable=wrong-import-position, import-error
from base import logger
from json import dumps
from pathlib import Path


def get_categories() -> list:
    """List of categories and their subcategories of the same name database tables.

    Returns:
        list: List of (sub-)categories.
    """
    categories = [
        {
            "Pets": [
                "Cats",
                "Cows",
                "Dogs",
                "Donkeys",
                "Ducks",
                "Horses",
                "Pigs",
                "Pigeons",
                "Sheeps",
                "Turkeys"
            ]
        }
    ]
    return categories


def main() -> None:
    """Main tool function.
    """
    basedir = Path(__file__).parent
    basename = "categories.json"
    categories = get_categories()
    with open(f"{basedir}/json/{basename}", "w", encoding="utf-8") as _file:
        _file.write(dumps(categories, indent=4, ensure_ascii=False))

    logger.info(f"[INFO] Successfully wrote all data to '{basedir}/json/{basename}'")


if __name__ == '__main__':
    main()
