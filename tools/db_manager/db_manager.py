# pylint: disable=import-error
from pathlib import Path
import sys
PROJECTDIR = str(Path(__file__).parents[2])
sys.path.append(PROJECTDIR)
from app.models.base import db
from app.models import Category, Subcategory, User, Advert
from tools.logger import logger

from data_generator import get_fake, generate_user, generate_advert
import docker
from flask import current_app, Flask
from flask_sqlalchemy import BaseQuery
from json import load, dumps
from shutil import rmtree
from subprocess import check_output, DEVNULL, TimeoutExpired, STDOUT, CalledProcessError
from typing import Optional, Any, Union, Type
from time import sleep


basename = Path(__file__).parent.name
logger.add(f"{PROJECTDIR}/tools/logs/{basename}.log", rotation="10 MB")


def run_cmd(
    cmd: list,
    stderr: int = DEVNULL,
    timeout: int = 15,
    debug: bool = False,
    **kwargs: Any
    ):
    """[Wrapper function for any bash commands executing]

    Args:
        cmd [list]: [Bash command]
        stderr ([int], optional): [Stderr stream management]. Defaults to DEVNULL.
        timeout ([int], optional): [Bash commands performing timeout]. Defaults to 15.
        debug (bool, optional): [Print more output]

    Returns:
        [str]: [Bash command result]
    """
    cmd = ["timeout", f"{timeout}"] + cmd

    if debug:
        logger.debug(f"Trying to run command: {cmd}")

    try:
        result = check_output(
            cmd,
            shell=False,
            universal_newlines=True,
            stderr=stderr,
            timeout=timeout,
            **kwargs
            )
    except TimeoutExpired as error:
        if timeout == 15:
            result = f"[FAIL] {error}"
            logger.error(result)
            return result
        raise error
    except CalledProcessError as exc:
        logger.error(f"[ERROR] {exc.output}")
        sys.exit(101)

    if result:
        logger.info(result.strip())

    return result


def is_container_running(container_name: str, docker_client: docker.DockerClient) -> Optional[bool]:
    """Verify the status of a container by it's name.

    Args:
        container_name (str): the name of the container.
        docker_client (docker.DockerClient): _description_

    Returns:
        Optional[bool]: boolean or None.
    """
    running = "running"
    try:
        container = docker_client.containers.get(container_name)
    except docker.errors.NotFound:
        logger.warning(f"No '{container_name}' running containers found.")
    else:
        container_state = container.attrs["State"]
        return container_state["Status"] == running
    return False


def create_init_migration(venv: list, app: Flask) -> None:
    """Creates initial migration via flask migrate.

    Args:
        venv (list): path to venv (python binary) in project.
        app (Flask): Pre-configured instance of Flask application.
    """
    migrations = Path(f"{PROJECTDIR}/migrations")
    if migrations.exists():
        answer = input("Migrations folder was found. Should I delete it? (YES/NO): ")
        if answer.lower() == "yes":
            rmtree(migrations)
            logger.info(f"Step 1: successfully removed '{migrations}' folder with its content")
    # flask db init
    init_cmd = ["-m", "flask", "db", "init", "-d", str(migrations)]
    run_cmd(venv + init_cmd, stderr=STDOUT, debug=True)
    # flask db migrate
    migrate_cmd = ["-m", "flask", "db", "migrate", "-m", "Initial migration", "-d", str(migrations)]
    run_cmd(venv + migrate_cmd, stderr=STDOUT, debug=True)
    # flask db upgrade
    upgrade_cmd = ["-m", "flask", "db", "upgrade", "-d", str(migrations)]
    run_cmd(venv + upgrade_cmd, stderr=STDOUT, debug=True)
    # create all tables
    with app.app_context():
        db.create_all()


def create_migration(venv: list, app: Flask) -> None:
    """Creates migrations via flask migrate.

    Args:
        venv (list): path to venv (python binary) in project.
        app (Flask): Pre-configured instance of Flask application.
    """
    # flask db migrate
    message = input("Enter migration message: ")
    migrate_cmd = ["-m", "flask", "db", "migrate", "-m", f"{message}", "-d", f"{PROJECTDIR}/migrations"]
    run_cmd(venv + migrate_cmd, stderr=STDOUT, debug=True)
    # flask db upgrade
    upgrade_cmd = ["-m", "flask", "db", "upgrade", "-d", f"{PROJECTDIR}/migrations"]
    run_cmd(venv + upgrade_cmd, stderr=STDOUT, debug=True)
    # create all tables
    with app.app_context():
        db.create_all()


def raw_exist(table: Union[Type[Category], Type[Subcategory]], name: str) -> BaseQuery:
    """Returns raw from required table if name field matches passed name string to function.

    Args:
        table (Union[Type[Category], Type[Subcategory]]): Table for searching raw.
        name (str): Value of name field which need to match.

    Returns:
        BaseQuery: Query result.
    """
    return table.query.filter_by(name=name).first()


def write_categories(app: Flask) -> None:
    """Writes content of categories.json to Category and Subcategory tables.

    Args:
        app (Flask): Pre-configured instance of Flask application.
    """
    with open(f"{PROJECTDIR}/tools/parser/json/categories.json", encoding="utf-8") as json_file:
        categories = load(json_file)

    with app.app_context():
        counter = 0
        for category_dict in categories:
            for category, subcategories in category_dict.items():
                if not raw_exist(Category, category):
                    category = Category(
                        name=category
                    )
                    db.session.add(category)            # pylint: disable=no-member
                    counter += 1
                for subcategory in subcategories:
                    if not raw_exist(Subcategory, subcategory):
                        subcategory = Subcategory(
                            name=subcategory,
                            parent=category
                        )
                        db.session.add(subcategory)     # pylint: disable=no-member
                        counter += 1
                db.session.commit()                     # pylint: disable=no-member
        logger.info(f"Step 2: '{counter}' raws of (sub-)categories were written")


def write_users(app: Flask) -> None:
    """Writes random generated (via Faker) users to User table.

    Args:
        app (Flask): Pre-configured instance of Flask application.
    """
    users_count = 10
    with app.app_context():
        rows_count = db.session.query(User).count()     # pylint: disable=no-member
        if rows_count != users_count:
            for _ in range(users_count):
                fake = get_fake("de_DE")
                user_dict = generate_user(fake)
                user = User(
                    firstname = user_dict["firstname"],
                    surename = user_dict["surename"],
                    password = user_dict["password"],
                    email = user_dict["email"],
                    birth = user_dict["birth"],
                    phone = user_dict["phone"],
                    city = user_dict["city"],
                    postcode = user_dict["postcode"],
                    address = user_dict["address"]
                )
                db.session.add(user)                    # pylint: disable=no-member
            db.session.commit()                         # pylint: disable=no-member
            logger.info(f"Step 2: '{users_count}' users rows were successfully written")
        else:
            logger.info("Step 2: No users rows were written")


def get_sub_id(animal_type: str) -> BaseQuery:
    """Queries id from Subcategory table by passed to function name field value.

    Args:
        animal_type (str): _description_

    Returns:
        BaseQuery: _description_
    """
    return Subcategory.query.filter_by(name=animal_type).first().id


def write_adverts(app: Flask) -> None:
    """Writes random generated (via Faker) adverts to Avdert table.

    Args:
        app (Flask): Pre-configured instance of Flask application.
    """
    with app.app_context():
        users_count = len(User.query.all())
        counter = 0
        for user_id in range(1, users_count + 1):
            fake = get_fake("de_DE")
            advert_dict = generate_advert(fake.animal(), fake.address())
            advert = Advert(
                name = advert_dict["name"],
                user_id = user_id,
                subcategory_id = get_sub_id(advert_dict["type"]),
                description = advert_dict["description"],
                price = advert_dict["price"],
                photos = dumps(advert_dict["photos"]),
                location = advert_dict["location"],
                zip = advert_dict["zip"],
                address = advert_dict["address"],
            )
            db.session.add(advert)                  # pylint: disable=no-member
            counter += 1
        db.session.commit()                         # pylint: disable=no-member
    logger.info(f"Step 2: '{counter}' adverts rows were successfully written")


def write_db_content(app: Flask) -> None:
    """Calles all write_* functions in right order.

    Args:
        app (Flask): Pre-configured instance of Flask application.
    """
    logger.info("Step 2: Start to write categories")
    write_categories(app)
    logger.info("Step 2: Start to write users")
    write_users(app)
    logger.info("Step 2: Start to write adverts")
    write_adverts(app)


def create_db(app: Flask, db_name: str) -> None:
    """Creates database according to detected engine in config.

    Args:
        app (Flask): _description_
        db_name (str): Name of database from config.
    """
    with app.app_context():
        engine_name = db.get_engine().name
    venv = [f"{PROJECTDIR}/.venv/bin/python3"]
    logger.info(f"Step 1: '{engine_name}' engine is detected")
    if engine_name == "sqlite":
        db_file = Path(f"{PROJECTDIR}/{db_name}")
        if not db_file.exists():
            logger.info(f"Step 1: No '{db_file}' was found. It will created by db_manager.py")
            create_init_migration(venv, app)
            logger.info(f"Step 1: '{db_file}' with table(-s) was successfully created")
        else:
            logger.info(f"Step 1: '{db_file}' was found. Further migration will created")
            create_migration(venv, app)
            logger.info(f"Step 1: Migration for '{db_file}' were successfully created")
    else:
        container_name = db_name + "-db-1"
        # Connect to Docker using the default socket or the configuration
        # in your environment
        docker_client = docker.from_env()
        # Or give configuration
        # docker_socket = "unix://var/run/docker.sock"
        # docker_client = docker.DockerClient(docker_socket)
        if not is_container_running(container_name, docker_client):
            logger.info(f"Step 1: Container '{container_name}' is not running")
            logger.info("Step 1: Start to pull images")
            cmd = ["/usr/bin/docker-compose", "-f", f"{PROJECTDIR}/docker-compose.yaml", "pull"]
            run_cmd(cmd, debug=True, stderr=STDOUT, timeout=60)

            logger.info(f"Step 1: Start to run '{container_name}'")
            cmd = ["/usr/bin/docker-compose", "-f", f"{PROJECTDIR}/docker-compose.yaml", "up", "-d"]
            run_cmd(cmd, debug=True, stderr=STDOUT)

            secs = 5
            logger.info(f"Step 1: Waiting {secs} seconds for postgres starting...")
            sleep(secs)
            create_init_migration(venv, app)
            logger.info(f"Step 1: Docker container '{container_name}' with table(-s) were \
                successfully created")
        else:
            logger.info(f"Step 1: Docker container '{container_name}' is already running. \
                Further migration will created")
            create_migration(venv, app)
            logger.info(f"Step 1: Migration with docker container '{container_name}' were \
                successfully created")


def get_db_name(app: Flask) -> str:
    """Gets database name from SQLALCHEMY_DATABASE_URI in config.

    Args:
        app (Flask): _description_

    Returns:
        str: Name of database from config.
    """
    with app.app_context():
        return current_app.config["SQLALCHEMY_DATABASE_URI"].split("/")[-1]
