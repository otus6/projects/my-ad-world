#!/usr/bin/env python3
from pathlib import Path
import sys
projectdir = Path(__file__).parents[2]
sys.path.append(str(projectdir))
from app import create_app                                          # pylint: disable=import-error
from tools.logger import logger                                     # pylint: disable=import-error

import argparse
from db_manager import create_db, get_db_name, write_db_content     # pylint: disable=no-name-in-module
from os import environ


def init_parser() -> argparse.Namespace:
    """It initializes argument parser for module.

    Returns:
        Namespace: Passed arguments to the module.
    """
    parser = argparse.ArgumentParser(
        description="db_manager generates content, write it in database and creates migrations \
            for this flask application."
    )
    parser.add_argument(
        "-d",
        "--dev",
        help=(
            "With this option tool write all generated content to database \
                for development environment. "
            "WARNING! By default (without this key) it works in production mode."
            ),
        action="store_true",
    )
    parser.add_argument(
        "-t",
        "--testing",
        help=(
            "With this option tool write all generated content to database \
                for testing environment. "
            "WARNING! By default (without this key) it works in production mode."
            ),
        action="store_true",
    )
    parser.add_argument(
        "-n",
        "--no-content",
        help=(
            "Only create an instance of db for non-existance databse or \
                perform migration if it exists. "
            ),
        action="store_true",
    )
    parser.add_argument(
        "-c",
        "--check",
        help=(
            "Check if database is exist and has a content according to database configuration. "
            ),
        action="store_true",
    )
    args = parser.parse_args()

    return args



@logger.catch(onerror=lambda _: sys.exit(1))
def main() -> None:
    """Main function, that runs this module.
    """
    if not environ.get("FLASK_APP"):
        environ["FLASK_APP"] = f"{projectdir}/run.py"
        logger.debug("Did not found ${FLASK_APP} env variable. \
            This variable was exported successfully")

    args = init_parser()
    app_mode = "production"
    if args.dev:
        app_mode = "development"
    elif args.testing:
        app_mode = "testing"
    environ["FLASK_ENV"] = app_mode
    app = create_app()
    db_name = get_db_name(app)

    logger.info("== Step 1 Database creation ==")
    create_db(app, db_name)
    logger.info("== Step 2 Writing content in database ==")
    write_db_content(app)


if __name__ == "__main__":
    main()
