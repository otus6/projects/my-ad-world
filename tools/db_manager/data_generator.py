from datetime import datetime
from faker import Faker
from faker.providers import BaseProvider
import itertools
from json import load, dumps
from pathlib import Path
import random
import string
from typing import TypeVar, Type
from werkzeug.security import generate_password_hash


T = TypeVar("T")

json_dir = f"{Path(__file__).parents[2]}/tools/parser/json"
with open(f"{json_dir}/animals.json", encoding="utf-8") as _file:
    animals = load(_file)

class AnimalProvider(BaseProvider):
    """Custom Provider for Faker to get animals random generator.

    Args:
        BaseProvider (class): class that provides base functionality for adding custom provider.
    """

    def animal(self) -> dict:
        """Returns random animal dictionary.

        Returns:
            dict: Animal dictionary.
        """
        animal = random.choice(animals)
        breed = random.choice(animal["breeds"])
        return breed


    @classmethod
    def get_breeds(cls: Type[T], animal_type: str) -> list:
        """Returns filtered list of breeds via animal_type.

        Args:
            cls (Type[T]): _description_
            animal_type (str): Value for animal type filter.

        Returns:
            list: Filtered list of animal breeds.
        """
        breeds = [animal["breeds"] for animal in animals if animal["type"] == animal_type]
        return breeds


    def cat(self) -> dict:
        """Return random cats breed.

        Returns:
            dict: Cats breed.
        """
        nested_list = AnimalProvider.get_breeds("Cats")
        cats = list(itertools.chain(*nested_list))
        return random.choice(cats)


    def dog(self) -> dict:
        """Return random dogs breed.

        Returns:
            dict: Dogs breed.
        """
        nested_list = AnimalProvider.get_breeds("Dogs")
        dogs = list(itertools.chain(*nested_list))
        return random.choice(dogs)


    def donkey(self) -> dict:
        """Return random donkeys breed.

        Returns:
            dict: Donkeys breed.
        """
        nested_list = AnimalProvider.get_breeds("Donkeys")
        donkeys = list(itertools.chain(*nested_list))
        return random.choice(donkeys)


    def duck(self) -> dict:
        """Return random ducks breed.

        Returns:
            dict: Ducks breed.
        """
        nested_list = AnimalProvider.get_breeds("Gooses")
        ducks = list(itertools.chain(*nested_list))
        return random.choice(ducks)


    def horse(self):
        """Return random horses breed.

        Returns:
            dict: Horses breed.
        """
        nested_list = AnimalProvider.get_breeds("Horses")
        horses = list(itertools.chain(*nested_list))
        return random.choice(horses)


    def cow(self) -> dict:
        """Return random cows breed.

        Returns:
            dict: Cows breed.
        """
        nested_list = AnimalProvider.get_breeds("Cows")
        cows = list(itertools.chain(*nested_list))
        return random.choice(cows)


    def sheep(self) -> dict:
        """Return random sheeps breed.

        Returns:
            dict: Sheeps breed.
        """
        nested_list = AnimalProvider.get_breeds("Sheeps")
        sheeps = list(itertools.chain(*nested_list))
        return random.choice(sheeps)


    def pig(self) -> dict:
        """Return random pigs breed.

        Returns:
            dict: Pigs breed.
        """
        nested_list = AnimalProvider.get_breeds("Pigs")
        pigs = list(itertools.chain(*nested_list))
        return random.choice(pigs)


    def pigeon(self) -> dict:
        """Return random pigeons breed.

        Returns:
            dict: Pigeons breed.
        """
        nested_list = AnimalProvider.get_breeds("Pigeons")
        pigeons = list(itertools.chain(*nested_list))
        return random.choice(pigeons)


    def turkey(self) -> dict:
        """Return random turkeys breed.

        Returns:
            dict: Turkeys breed.
        """
        nested_list = AnimalProvider.get_breeds("Turkeys")
        turkeys = list(itertools.chain(*nested_list))
        return random.choice(turkeys)


def generate_password() -> str:
    """Generates strong password.

    Returns:
        str: Password value.
    """
    length = 10

    #define data
    lower = string.ascii_lowercase
    upper = string.ascii_uppercase
    num = string.digits
    symbols = string.punctuation
    #string.ascii_letters

    #combine the data
    all_data = lower + upper + num + symbols

    #use random
    temp = random.sample(all_data,length)

    #create the password
    password = "".join(temp)
    return password


def generate_user(fake: Faker) -> dict:
    """Generate random user via Faker.

    Args:
        fake (Faker): Faker instance.

    Returns:
        dict: Generated user dictionary.
    """
    birth = fake.date_between(
        start_date=datetime(1968,1,1).date(), end_date=datetime(2004,1,1).date()
        )
    password = generate_password_hash(generate_password())
    return {
        "firstname": fake.name(),
        "surename": fake.last_name(),
        "password": password,
        "email": fake.email(),
        "birth": birth,
        "phone": fake.phone_number(),
        "city": fake.city(),
        "postcode": fake.postcode(),
        "address": fake.street_address()
    }


def generate_advert(animal: dict, address: str) -> dict:
    """Generates random advert.

    Args:
        animal (dict): Animal data.
        address (str): Location of advert.

    Returns:
        dict: _description_
    """
    title_prefixes = {
        "buy": {
            "start": ["Ich kaufe", "kaufe"],
            "end": ["An- und Verkauf", "Ankauf", "gesucht"]
        },
        "sell": {
            "start": ["Ich verkaufe", "verkaufe"],
            "end": ["zum Verkauf"]
        }
    }
    age = random.randrange(1,11)
    breed = animal['name']
    breed_description = animal['description']
    photos = animal['images']

    type_ = random.choice(list(title_prefixes))
    range_ = random.choice(list(title_prefixes[type_]))
    prefix = random.choice(list(title_prefixes[type_][range_]))
    price = random.randrange(50, 200)
    point = breed
    if type_ == "sell":
        point = f"{breed} {age} Monate"

    if range_ == "start":
        description = f"{prefix} {point}. {price} Euro pro Stück. {breed_description}"
    else:
        description = f"{point} {prefix}. {price} Euro pro Stück. {breed_description}"
    return {
        "name": point,
        "description": description,
        "photos": photos,
        "address": address.split("\n")[0],
        "zip": address.split("\n")[1].split()[0],
        "location": " ".join(address.split("\n")[1].split()[1:]),
        "price": random.randrange(50, 200),
        "type": animal["type"]
    }


def get_fake(locale: str) -> Faker:
    """Returns faker with suitable locale and added custom provider.

    Args:
        locale (str): Required locale.

    Returns:
        Faker: Faker instance.
    """
    fake = Faker(locale)
    fake.add_provider(AnimalProvider)
    return fake


def main() -> None:
    """Main tool function.
    """
    fake = get_fake("de_DE")
    advert = generate_advert(fake.animal(), fake.address())
    print(dumps(advert, indent=4, ensure_ascii=False))


if __name__ == '__main__':
    main()
