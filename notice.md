# Views
- Home
- Login
- Register
- Profile/Settings
- Create advert
- Edit advert
- URL результатов поиска генерятся динамически


`docker exec -it my-ad-world_db_1 psql -U flask my-ad-world`
`docker-compose down && docker volume rm my-ad-world_postgres_data`


# Название и идея

my-ad-world.de - сайт объявлений о продажах по нескольким категориям. Территориальное ограничение это ФРГ. 
Список категорий: 
    - Pets
        - Cats
        - Dogs
        - Small animals
        - Birds
    - For Pets
        - Homes
        - Feed
        - Accessories
        - Bioadditives and medicines
        - Toys
        - Clothes
    - Give away and exchange
        - Give away
        - Exchange
    - Journey
        - Ammunition
        - Accessories
        - Bags
        - Glasses
    - Food
        - Health
        - Vegan
        - Meat
        - Water and drinks
        - Bakery


# БД

Таблички:
- USER
- AD


# AD
- id
- name
- category_id ???
- subcategory_id
- price
- description
- photos        (optional)
- location
- zip_code      (optional)
- address       (optional)
- contacts      (optional)
- publish_date

# USER
- id 
- firstname
- surname
- password
- email
- phone
- age       (optional)
- address   (optional)
- icon      (optional)
- registered_date


# Category
- id
- name

# Subcategory
- id 
- name
- category_id


1. Какие категории? Где храню?
2. Найти базу с городами и землями Германии для AD location и USER address
3. Сделать скрипт, который заберет все категории и сабкатегории из JS на сайте и положит в json (потом можно сразу в базу)
4. Создать модели Category и subcategory со связью
5. Определиться с СУБД для категорий и поднять ее


# Compose

services:
    - nginx
    - app
    - app-db
    - location-db

# Что должно быть?
1. Система авторизации
2. Настройки\профиль
3. Создать объявление
4. Редактировать объявление
5. Поиск и фильтрация объявлений на сайте 
6. Комментарии к объявлению? (опционально)
7. Уведомление о комментарии на почту?  (опционально)
8. Два языка: eng/de (опционально)

